/*
* Add edit delete rows dynamically using jquery and php
* http://www.amitpatil.me/
*
* @version
* 2.0 (4/19/2014)
*
* @copyright
* Copyright (C) 2014-2015
*
* @Auther
* Amit Patil
* Maharashtra (India)
*
* @license
* This file is part of Add edit delete rows dynamically using jquery and php.
*
* Add edit delete rows dynamically using jquery and php is freeware script. you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Add edit delete rows dynamically using jquery and php is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this script.  If not, see <http://www.gnu.org/copyleft/lesser.html>.
*/

// init variables
var trcopy1;
var editing1 = 0;
var tdediting1 = 0;
var editing1trid = 0;
var editing1tdcol = 0;
var inputs1 = ':checked,:selected,:text,textarea';

$(document).ready(function() {

	// set images for edit and delete
	$(".eimage1").attr("src", editImage1);
	$(".dimage1").attr("src", deleteImage1);

	// init table1
	blankrow1 = '<tr valign="top" class="inputform1"><td></td>';
	for ( i = 0; i < columns1.length; i++) {
if(i== columns1.length-1){
	input = createInput1(i, "");
		blankrow1 += '<td class="aid">' + input + '</td>';
}
	else{
		// Create input element as per the definition
		input = createInput1(i, "");
		blankrow1 += '<td class="ajax1Req1">' + input + '</td>';

	}
}
	blankrow1 += '<td><a href="javascript:;" class="' + savebutton1 + '"><img src="' + saveImage1 + '"></a></td></tr>';

	// append blank row at the end of table1
	$("." + table1).append(blankrow1);

	// Delete record
	$(document).on("click", "." + deletebutton1, function() {
		var id = $(this).attr("id");
		if (id) {
			if (confirm("Do you really want to delete record ?"))
				ajax1("rid1=" + id, "del");
		}


	});

	// Add new record
	$("." + savebutton1).on("click", function() {
		var validation = 1;

		var $inputs1 = $(document).find("." + table1).find(inputs1).filter(function() {
			// check if input element is blank ??

			$(this).addClass("success");

			return $.trim(this.value);
		});

		var array = $inputs1.map(function() {
			return this.value;
		}).get();

		var serialized = $inputs1.serialize();
		if (validation == 1) {
			ajax1(serialized, "save");
		}
	});

	// Add new record
	$(document).on("click", "." + editbutton1, function() {
		var id = $(this).attr("id");
		if (id && editing1 == 0 && tdediting1 == 0) {
			// hide editing1 row, for the time being
			$("." + table1 + " tr:last-child").fadeOut("fast");

			var html;
			html += "<td>" + $("." + table1 + " tr[id=" + id + "] td:first-child").html() + "</td>";
			for ( i = 0; i < columns1.length; i++) {
				// fetch value inside the TD and place as VALUE in input field
				var val = $(document).find("." + table1 + " tr[id=" + id + "] td[class='" + columns1[i] + "']").html();

				input = createInput1(i, val);
				if (columns1[i] == "aid") {
					html += '<td class="aid" >' + input + '</td>';
				} else {
					html += '<td>' + input + '</td>';
				}
			}

			html += '<td><a href="javascript:;" id="' + id + '" class="' + updatebutton1 + '"><img src="' + updateImage1 + '"></a> <a href="javascript:;" id="' + id + '" class="' + cancelbutton1 + '"><img src="' + cancelImage1 + '"></a></td>';

			// Before replacing the TR contents, make a copy so when user clicks on
			trcopy1 = $("." + table1 + " tr[id=" + id + "]").html();
			$("." + table1 + " tr[id=" + id + "]").html(html);

			// set editing1 flag
			editing1 = 1;
		}
	});

	$(document).on("click", "." + cancelbutton1, function() {
		var id = $(this).attr("id");
		$("." + table1 + " tr[id='" + id + "']").html(trcopy1);
		$("." + table1 + " tr:last-child").fadeIn("fast");
		editing1 = 0;
	});

	$(document).on("click", "." + updatebutton1, function() {
		id = $(this).attr("id");
		var $inputs1 = $(document).find("." + table1).find(inputs1).filter(function() {
			return $.trim(this.value);
		});

		var array = $inputs1.map(function() {
			return this.value;
		}).get();
		serialized = $inputs1.serialize();
		ajax1(serialized + "&rid1=" + id, "update");
		// clear editing1 flag
		editing1 = 0;
	});

	// td doubleclick event
	$(document).on("dblclick", "." + table1 + " td", function(e) {
		// check if any other TD is in editing1 mode ? If so then dont show editing1 box
		//alert(tdediting1+"==="+editing1);

		
		var isEditing1form = $(this).closest("tr").attr("class");
		if (tdediting1 == 0 && editing1 == 0 && isEditing1form != "inputform1"  ) {
			editing1trid = $(this).closest('tr').attr("id");
			editing1tdcol = $(this).attr("class");
			var text = $(this).html();
			var tr = $(this).parent();
			var tbody = tr.parent();
			for (var j = 0; j < tr.children().length; j++) {
				if (tr.children().get(j) == this) {
					var column = j;
					break;
				}
			}

			// decrement column value by one to avoid sr no column
			column--;
			//alert(column+"==="+placeholder1[column]);
			if (column <= columns1.length) {
				var text = $(this).html();
				//alert(text);
				input = createInput1(column, text);
				$(this).html(input);
				$(this).find(inputs1).focus();
				tdediting1 = 1;
			}
		

	}
	});

	// td lost focus event

	$(document).on("blur", "." + table1 + " td", function(e) {
		if (tdediting1 == 1) {
			var newval1 = $("." + table1 + " tr[id='" + editing1trid + "'] td[class='" + editing1tdcol + "']").find(inputs1).val();
			ajax1(editing1tdcol + "=" + newval1 + "&rid1=" + editing1trid, "updatetd");
		}
	});

});

createInput1 = function(i, str) {
	str = typeof str !== 'undefined' ? str : null;
	if (inputType1[i] == "text") {
		if (columns1[i] == "aid") {
			input = '<input type=' + inputType1[i] + ' name=' + columns1[i] + ' placeholder="' + placeholder1[i] + '" value="' + document.aid2 + '" >';
		} else {
			input = '<input type=' + inputType1[i] + ' name=' + columns1[i] + ' placeholder="' + placeholder1[i] + '" value=' + str + ' >';
		}
	} else if (inputType1[i] == "textarea") {
		input = '<textarea name=' + columns1[i] + ' placeholder="' + placeholder1[i] + '">' + str + '</textarea>';
	}
	return input;
}
ajax1 = function(params, action) {
	$.ajax({
		type : "POST",
		url : "ajax1.php",
		data : params + "&action=" + action,
		dataType : "json",
		success : function(response) {
			switch(action) {
			case "save":
				var seclastRow1 = $("." + table1 + " tr").length;

				if (response.success == 1) {
					var html = "";

					html += "<td>" + parseInt(seclastRow1 - 1) + "</td>";
					for ( i = 0; i < columns1.length; i++) {
						html += '<td class="' + columns1[i] + '">' + response[columns1[i]] + '</td>';
					}
					html += '<td><a href="javascript:;" id="' + response["id"] + '" class="ajaxEdit1"><img src="' + editImage1 + '"></a> <a href="javascript:;" id="' + response["id"] + '" class="' + deletebutton1 + '"><img src="' + deleteImage1 + '"></a></td>';
					// Append new row as a second last row of a table1
					$("." + table1 + " tr").last().before('<tr id="' + response.id + '">' + html + '</tr>');

					if (effect == "slide") {
						// Little hack to animate TR element smoothly, wrap it in div and replace then again replace with td and tr's ;)
						$("." + table1 + " tr:nth-child(" + seclastRow1 + ")").find('td').wrapInner('<div style="display: none;" />').parent().find('td > div').slideDown(700, function() {
							var $set = $(this);
							$set.replaceWith($set.contents());
						});
					} else if (effect == "flash") {
						$("." + table1 + " tr:nth-child(" + seclastRow1 + ")").effect("highlight", {
							color : '#acfdaa'
						}, 100);
					} else
						$("." + table1 + " tr:nth-child(" + seclastRow1 + ")").effect("highlight", {
							color : '#acfdaa'
						}, 1000);

					// Blank input fields

					$(document).find("." + table1).find(inputs1).filter(function() {
						// check if input element is blank ??
						if (this.value != document.aid2) {
							this.value = "";
						}
						$(this).removeClass("success").removeClass("error");
					});
				}
				break;
			case "del":
				var seclastRow1 = $("." + table1 + " tr").length;

				if (response.success == 1) {
					$("." + table1 + " tr[id='" + response.id + "']").effect("highlight", {
						color : '#f4667b'
					}, 500, function() {
						$("." + table1 + " tr[id='" + response.id + "']").remove();
					});
				}
				break;
			case "update":
				$("." + cancelbutton1).trigger("click");
				for ( i = 0; i < columns1.length; i++) {
					$("tr[id='" + response.id + "'] td[class='" + columns1[i] + "']").html(response[columns1[i]]);
				}
				break;
			case "updatetd":
				//$("."+cancelbutton1).trigger("click");
				var newval1 = $("." + table1 + " tr[id='" + editing1trid + "'] td[class='" + editing1tdcol + "']").find(inputs1).val();

				//alert($("."+table1+" tr[id='"+editing1trid+"'] td[class='"+editing1tdcol+"']").html());
				$("." + table1 + " tr[id='" + editing1trid + "'] td[class='" + editing1tdcol + "']").html(newval1);

				//$("."+table1+" tr[id='"+editing1trid+"'] td[class='"+editing1tdcol+"']").html(newval1);
				// remove editing1 flag
				tdediting1 = 0;
				$("." + table1 + " tr[id='" + editing1trid + "'] td[class='" + editing1tdcol + "']").effect("highlight", {
					color : '#acfdaa'
				}, 1000);
				break;
			}
		},
		error : function() {
			alert("Unexpected error, Please try again");
		}
	});
}