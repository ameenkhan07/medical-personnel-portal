<?php


$DB_NAME = 'orfom';

$DB_HOST = 'localhost';

$DB_USER = 'root';
$DB_PASS = '';


  $mysqli = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);

  if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
  }

   
  $result = $mysqli->query('SELECT * FROM chart');

	$rows = array();
  $table = array();
  $table['cols'] = array(

  

    array('label' => 'Weekly Task', 'type' => 'string'),
    array('label' => 'Percentage', 'type' => 'number')
);

//print_r ($table);

    /* Extract the information from $result */
    foreach($result as $r) {

      $temp = array();
	 //print_r ($temp);
      // The following line will be used to slice the Pie chart

      $temp[] = array('v' => (string) $r['weekly_task']); 
	  //print_r ($temp);

      // Values of the each slice

      $temp[] = array('v' => (int) $r['percentage']); 
	  //print_r ($temp);
      $rows[] = array('c' => $temp);
	  
    }
	//var_dump($rows);

$table['rows'] = $rows;

// convert data into JSON format
$jsonTable = json_encode($table);
//echo $jsonTable;


?>


<html>
  <head>
    <!--Load the Ajax API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script type="text/javascript">

    // Load the Visualization API and the piechart package.
    google.load('visualization', '1', {'packages':['corechart']});

    // Set a callback to run when the Google Visualization API is loaded.
    google.setOnLoadCallback(drawChart);

    function drawChart() {

      // Create our data table out of JSON data loaded from server.
      var data = new google.visualization.DataTable(<?=$jsonTable?>);
      var options = {
           title: 'My Weekly Plan',
          is3D: 'true',
          width: 800,
          height: 600
        };
      // Instantiate and draw our chart, passing in some options.
      // Do not forget to check your div ID
      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    }
    </script>
  </head>

  <body>
    <div id="chart_div"></div>
  </body>
</html>