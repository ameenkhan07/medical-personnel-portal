<!DOCTYPE html>
<?php session_start();
date_default_timezone_set('Asia/Calcutta');?>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="css/style2.css">


  <meta charset="utf-8">
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
  <link href="calendar/css/ui-darkness/jquery-ui-1.10.4.custom.css" rel="stylesheet">

  <script src="calendar/js/jquery-1.10.2.js"></script>
  <script src="calendar/js/jquery-ui-1.10.4.custom.js"></script>
  <script type="text/javascript" language="javascript" src="fee.js"></script>
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
  <script type="text/javascript" src="doctorsearch.js"></script>

  <script type="text/javascript" src="../forms3/scripts/countries.js"></script>
  
  <style>

    .menubox{
      text-align:center;margin:2%;width:130px;height:30px;border-width:1px; border-color:#fff; border-style:solid; float :left;font-size: 12px;
      letter-spacing: 0.6px;font-family: 'proxima-nova', 'Calibri', 'Cantarell', sans-serif;color:#fff

    }
    .menubox:hover{
      text-align:center;margin:2%;width:130px;height:30px;border-width:1px; border-color:#fff; border-style:solid; float :left;font-size: 12px;
      letter-spacing: 0.6px;font-family: 'proxima-nova', 'Calibri', 'Cantarell', sans-serif;color:#fff; background-color: rgba(181, 174, 174, 0.67);

    }
    input[type=text] {
      font-family: Helvetica, Arial, 'lucida grande',tahoma,verdana,arial,sans-serif;
      width: 244px;
      height: 20px;
      line-height: 20px;
      margin-top: 4.8%;
      margin-left: 7.4%;
      background-color: #fff;
      border-width: 1px;
      border-color: #fff;
      border-style: solid;
      background-color: #fff;
      font-size: 13px;
      border-radius: 0px;
      color: #4e5665;
    }
    input[type=checkbox] {
      position: relative;top: 4px;
    }
    .daybox{
      position: relative;
      left: -13px;
      top: -9px;
      color:#e9eaed;
    }
    .button{ width:40px;
      height:40px;
      background-image:url(img/Search.png);
      background-color:#3b988e;
      background-repeat:  no-repeat;

    }
    .tab1{
     width: 100px;
     height:40px;
     border:1px;
   }
   .tab1:hover{
     cursor: pointer;
     width: 100px;
     height:40px;
     border:1px;
     background-color: #fff;
   }
   .demoHeaders {
    margin-top: 2em;
  }
  #dialog-link {
    padding: .4em 1em .4em 20px;
    text-decoration: none;
    position: relative;
  }
  #dialog-link span.ui-icon {
    margin: 0 5px 0 0;
    position: absolute;
    left: .2em;
    top: 50%;
    margin-top: -8px;
  }
  #icons {
    margin: 0;
    padding: 0;
  }
  #icons li {
    margin: 2px;
    position: relative;
    padding: 4px 0;
    cursor: pointer;
    float: left;
    list-style: none;
  }
  #icons span.ui-icon {
    float: left;
    margin: 0 4px;
  }
  .fakewindowcontain .ui-widget-overlay {
    position: absolute;
  }
</style> 


<script>
  $("#autocomplete").autocomplete({
    source : function(request, response) {
      var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(request.term), "i");
      response($.grep(tags, function(item) {
        return matcher.test(item);
      }));
    }
  }); 
</script>
<script type="text/javascript">
  function call()
  {setTimeout('get()',2000);
}


function search()
{ 
  var  xhr=new XMLHttpRequest();

  var ht="<span class='label' style='position: relative;top: 13px;color: #e9eaed;font-family: Helvetica, Arial, lucida grande,tahoma,verdana,arial,sans-serif;margin-left: 21px;'>Hospital Name </span> \
  <input type='text'id='hospital' name='name' placeholder='eg:AIIMS' > \
  <span class='label' style='position: relative;top: 13px;color: #e9eaed;font-family: Helvetica, Arial, lucida grande,tahoma,verdana,arial,sans-serif;margin-left: 21px;'>Location</span>\
  <input type='text' name='location'id='location' placeholder='eg:Hauz Khas'> \
  <button onclick='hossearch()'style='width:100px;height:auto;color:#726486;margin-left:20px;margin-top:20px;'>Search</button>";

  document.getElementById("searcharea").innerHTML=ht;
  xhr.onreadystatechange=function() {
    if (xhr.readyState==4 && xhr.status==200) {
      document.getElementById("result").innerHTML=xhr.responseText;
    }
  }
  xhr.open('GET','hospitalfilter.php',true);
  xhr.send();

}
function doctor()
{   var  xhr=new XMLHttpRequest();


  var ht="<span class='label' style='position: relative;top: 13px;color: #e9eaed;font-family: Helvetica, Arial, lucida grande,tahoma,verdana,arial,sans-serif;margin-left: 21px;'> Doctor Name</span> \
  <input type='text' id='autocomplete'name='name' placeholder='eg:Mukesh' value='<?php if(isset($_GET['name'])) echo $_GET['name'];?>' />\
  <span class='label' style='position: relative;top: 13px;color: #e9eaed;font-family: Helvetica, Arial, lucida grande,tahoma,verdana,arial,sans-serif;margin-left: 21px;'>Speciality</span>\
  <input type='text' id='autocomplete2' name='special' placeholder='eg:Neurologist' value='<?php if(isset($_GET['special'])) echo $_GET['special'];?>'/>\
  <span class='label' style='position: relative;top: 13px;color: #e9eaed;font-family: Helvetica, Arial, lucida grande,tahoma,verdana,arial,sans-serif;margin-left: 21px;'>Qualification</span>\
  <input type='text' id='qual' name='qual' placeholder='eg:MBBS' />\
  <span class='label' style='position: relative;top: 13px;color: #e9eaed;font-family: Helvetica, Arial, lucida grande,tahoma,verdana,arial,sans-serif;margin-left: 21px;'>Location</span>\
  <input type='text' id='add' name='add' placeholder='eg:Hauz Khas' value='<?php if(isset($_GET['add'])) echo $_GET['add']; ?>'/>\
  <span class ='any' style='color:#e9eaed;margin-left: 13px;position: relative;top: 20px;'/>\
  <input type='checkbox' name='any' value='any' id='any_day' checked  style='left:8px'>\
  <label class='daybox'for='any_day'  role='button' >ANY</label>  \
</span></br>\
<span class='day' style=' margin-left:66px;width:30px; height:30px; '>\
  <input type='checkbox' name='day[]' value='0' id='monday' style='left: -1px;'>\
  <label class='daybox' style='left: -21px;' for='monday' role='button'>M</label>\
  <input type='checkbox' name='day[]' value='1' id='tuesday' style='left: -9px;'>\
  <label class='daybox' style='left: -27px;' for='tuesday' role='button'>T</label>\
  <input type='checkbox' name='day[]' value='2' id='wednesday' style='    left: -14px;'>\
  <label class='daybox' style='left: -35px;' for='wednesday' role='button'>W</label>\
  <input type='checkbox' name='day[]' value='3' id='thursday' style='    left: -20px;'>\
  <label class='daybox' style='left: -43px;' for='thursday' role='button'>Th</label>\
  <input type='checkbox' name='day[]' value='4' id='friday' style='left: -32px;'>\
  <label class='daybox' style='left: -50px;' for='friday' role='button'>F</label>\
  <input type='checkbox' name='day[]' value='5' id='saturday' style='left: -37px;'>\
  <label class='daybox' style='left: -54px;' for='saturday' role='button'>S</label>\
  <input type='checkbox' name='day[]' value='6' id='sunday' style='left: -42px;'>\
  <label class='daybox' style='left: -59px;' for='sunday' role='button'>S</label></span>\
  <input type='range' id='feetext' name='fee' min='0' max='2500' value='2500' step='100'  style='  width: 248px;position: relative;left: 22px;top: 11px;' onchange='updateTextInput(this.value);'/> \
  <label style='position:relative;color:#000;left: -230px;top: 25px;'/>0 -</label>\
  <input style='border: none;position: relative;background-color: #3B988E;left: 47px;top: 6px;width:63px; font-size:14px' id='textInput' value='2500' > \
  <button onclick='doctorsearch()' style='width:100px;height:auto;color:#726486;position: relative;top: 30px;left: -45px;' >Search </button>";

  document.getElementById("searcharea").innerHTML=ht;
  xhr.onreadystatechange=function() {
    if (xhr.readyState==4 && xhr.status==200) {
      document.getElementById("result").innerHTML=xhr.responseText;
    }
  }
  xhr.open('GET','doctorfilter.php',true);
  xhr.send();

}
</script>
<script type="text/javascript">
  function updateTextInput(val) {
    document.getElementById('textInput').value=val; 
  }
</script>

<script type="text/javascript">

  $( '#autocomplete' ).autocomplete({
    source: function( request, response ) {
      var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
      response( $.grep( tags, function( item ){
        return matcher.test( item );
      }) );
    }
  });
  $( "#autocomplete2" ).autocomplete({
    source: function( request, response ) {
      var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
      response( $.grep( tags2, function( item ){
        return matcher.test( item );
      }) );
    }
  });

</script>  
</head>
<body style="background-color: #e9eaed; padding:0px; margin:0px">
  <header style='background-color: #3b9885; width:100%;height:50px;position:fixed; top:-1px; z-index:1000' >


    <?php


    $dbhost = 'localhost';
// Unlikely to require changing
    $dbname = 'orfom';
// Modify these...
    $dbuser_name = 'orfom';
// ...variables according
    $dbpass = 'awerty56';
// ...to your installation

    mysql_connect($dbhost, $dbuser_name, $dbpass) or die(mysql_error());
    mysql_select_db($dbname) or die(mysql_error());

    include 'function.php';

    if (isset($_SESSION['user_name'])) {
     $user_name = $_SESSION['user_name'];
     $loggedin = TRUE;
     $user_namestr = " ($user_name)";
   } else
   $loggedin = FALSE;
   if($loggedin)
   {
    $subquery = "SELECT * FROM users WHERE user_name='$user_name'";
    $subresult = mysql_query($subquery);
    if (!$subresult)
     die("Database access failed: " . mysql_error());
   $subrow = mysql_fetch_row($subresult);
   $uid =$subrow[0];
   echo  "<div> </div><span class='profilepic'><img src='images/$user_name.jpg' style='width:32px; height: 32px;margin-top:8px; border-radius:3px;'/></span> 
   <span class='profilename' > Mr. $user_name</span><span class='profileid'> ID: $uid</span><span class='navmenuwrapper'> </span>";
   echo "<div style='position: relative;margin-left: 36%;margin-top: -53px; '>
   <a href='userpage.php' ><div class= 'menubox'><div style='margin-top:8px'>Appointment</div></div></a>
   <a href='ordermedicine' ><div class= 'menubox'><div style='margin-top:8px'>Find Chemist</div></div></a>
   <a href='findPatho' ><div class= 'menubox'><div style='margin-top:8px'>Find Labs</div></div></a>
   <a href='timeline.php' ><div class= 'menubox'><div style='margin-top:8px'>Timeline</div></div></a></div>  
   <div class='settings'><a href='ulogout.php' ><img src='images/settings2.png' style='border-radius:3px;' ></a></div>
   ";

 }
 ?>
 <?php 
 if(!$loggedin){

  echo   "<div class='arbit'> </div><div style='position:relative;  margin-left:60%;padding-top:0px '>
  <div class= 'menubox'>
    <div style='margin-top:8px'>GET LISTED</div>
  </div>
  <div class= 'menubox'>
    <div style='margin-top:8px'>CONTACT US</div>
  </div>
  <a href='http://localhost/~praveenkumar/orfom/login/user_login/uindex.php' ><div class= 'menubox'>
    <div style='margin-top:8px'>LOGIN</div>
  </div></a>
</div>  
";
}?>
</header>
<div class ='container1'> <div class='whitespace'></div><div class ='container2'> <div class='whitespace2'></div>
<?php
$con = mysql_connect('localhost','orfom','awerty56') or die('Error in connection');
mysql_select_db('orfom',$con);
if (isset($_GET['view'])) {
  $view = sanitizeString($_GET['view']);
  showProfile($view);
  die("</div></body></html>");
}
if (isset($_GET['viewinfo'])) {
  $view = sanitizeString($_GET['viewinfo']);
  showHosinfo($view);
  die("</div></body></html>");
}
if (isset($_GET['viewdoctor']))
 {$view = sanitizeString($_GET['viewdoctor']);
showDoctor($view);
die("</div></body></html>");
}
?>
<div id="feeshow"></div>

<?php  if (isset($_GET['appointment'])) :

  $user_name = sanitizeString($_GET['appointment']);

  $result = mysql_query("SELECT * FROM relation WHERE DID='$user_name'");
  $result3 = mysql_query("SELECT * FROM doctors WHERE DID='$user_name'");
  $rows = mysql_num_rows($result);
  $row = mysql_fetch_array($result3);
  $data = $name1 = $tel=$dep=$yoe=$gen=$DID='';
  $name1= $row['name'];
  $tel= $row['tel'] ;
  $dep= $row['dep'] ;
  $yoe= $row['yoe'];
  $gen= $row['gender'];
  $DID= $row['DID']; 
  $PlaceOH=$row['PlaceOH'];
  ?>

  <div class='appointmentc' style='width:900px;margin-left:0px;'><div class= 'appointmentpic'><img src='doc_image/<?php echo $tel?>.jpg' style='width:84px; height: 84px; border-radius:4px;'></img></div>
  <div class='appointmentdetails' style='margin-top:-80px; margin-left:100px;'>
    <div class='appointmentname1' >Dr.<?php echo $name1?></div>
    <div class='appointmentname' style='margin-top:11px;'>Dr.<?php echo $name1?></div>
    <div class='appointmentname' style='margin-top:28px;'><?php echo $dep?></div>
    <div class='appointmentname' style='margin-top:68px;'><?php echo $tel?></div>
  </div>
  <div style="width:0px;position: relative;left: 87%;"><a href='userpage.php?view=<?php echo $tel?>' style='text-decoration:none; color: #fff;'><div class='info' >Info</div></a></div>
</div>

<?php
for($i=0;$i<$rows;$i++):

  $row1 = mysql_fetch_array($result);
$name1 = $tel=$dep=$yoe=$gen=$NOH=$HID=$DID='';
$DID=$row1['DID'];
$HID=$row1['HID'];
$NOH=$row1['NOH'];
if(!$loggedin)
  $uid=0;
?>

<div id="feeshow"></div>

<?php echo "<script type='text/javascript'>


$(function() {



  $( '.datepicker$i' ).datepicker({
    inline: true,
    minDate: '0',
    maxDate: '+1m +1w +3d',
    dateFormat: 'dd-mm-yy',

    onSelect: function(dateText, inst) {

      var date = $(this).datepicker('getDate');
      var yr=date.getUTCFullYear();
      var dt=date.getUTCDate()+1;
      var mt=date.getUTCMonth()+1; 
      var dayOfWeek = date.getUTCDay();
      var xmlhttp;
      if (window.XMLHttpRequest) 
      {
                     // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
      }
      else {// code for IE6, IE5
        xmlhttp=new ActiveXObject('Microsoft.XMLHTTP');
      }
      xmlhttp.onreadystatechange=function() {

       if (xmlhttp.readyState==4 && xmlhttp.status==200) {

        document.getElementById('txt" . $i . "').innerHTML=xmlhttp.responseText;
      }
    }

    xmlhttp.open('GET','getuser.php?hid=$HID'  +'&date='+date+ '&dayOfWeek='+ dayOfWeek +'&DID=$DID'+'&uid=$uid' +'&yr='+ yr +'&dt='+ dt +'&mt='+ mt ,true);
    xmlhttp.send();

    $( this ).css( 'background-color', '' );

  }

});

    // Hover states on the static widgets
$( '#dialog-link, #icons li' ).hover(
  function() {
    $( this ).addClass( 'ui-state-hover' );
  },
  function() {
    $( this ).removeClass( 'ui-state-hover' );
  }
  );
});

</script>";
?>

<div class='maincont'style="margin-top:10px; display:flex" align="center">

  <div class="dateAndDetails" align="left">
    <div class='NOH' style="color:#00624E"> Name of Hospital: </br><?php echo $NOH?></div>
    <div class="PlaceOH"></br><?php echo $PlaceOH ?> </div></br>

    <div class='timingODinH'> <span style="color:#00624E">Timing </span></br>

      <?php 


      $result2 = queryMysql ("SELECT * FROM doctors WHERE DID='$user_name' AND HID='$HID' AND day=0 ORDER BY start_time ASC ");
      $rows2 = mysql_num_rows($result2);
      if ($rows2>0):
        echo "Mon:</br>"; 
      for($j=0;$j<$rows2;$j++):
        $fee=$start_time=$end_time=$day='';
      $row2 = mysql_fetch_array($result2);
      $start_time= $row2['start_time'];
      $end_time= $row2['end_time'];
      $day= $row2['day'];
      $open_time = strtotime($start_time);
      $close_time = strtotime($end_time);
      $output = date("g:i:a",$open_time);
      $output1 = date("g:i:a",$close_time);?>
      <?php
      if ($day == 0) {echo $output . "-" . $output1 . '</br>';
    }
    ?> 
  <?php endfor; ?><?php endif; ?>
  <?php 

  $result2 = queryMysql ("SELECT * FROM doctors WHERE DID='$user_name' AND HID='$HID' AND day=1 ORDER BY start_time ASC ");
  $rows2 = mysql_num_rows($result2);
  if ($rows2>0):
    echo "Tue:</br>";
  for($j=0;$j<$rows2;$j++):
    $fee=$start_time=$end_time=$day='';
  $row2 = mysql_fetch_array($result2);
  $start_time= $row2['start_time'];
  $end_time= $row2['end_time'];
  $day= $row2['day'];
  $open_time = strtotime($start_time);
  $open_time = strtotime($start_time);       
  $close_time = strtotime($end_time);
  $output = date("g:i:a",$open_time);
  $output1 = date("g:i:a",$close_time);
  ?>
  <?php
  if ($day == 1) {echo $output . "-" . $output1 . '</br>';
}
?>
<?php endfor; ?><?php endif; ?>

<?php 

$result2 = queryMysql ("SELECT * FROM doctors WHERE DID='$user_name' AND HID='$HID' AND day=2 ORDER BY start_time ASC ");
$rows2 = mysql_num_rows($result2);
if ($rows2>0):
  echo "Wed:</br>"; 
for($j=0;$j<$rows2;$j++):
  $fee=$start_time=$end_time=$day='';
$row2 = mysql_fetch_array($result2);
$start_time= $row2['start_time'];
$end_time= $row2['end_time'];
$day= $row2['day'];
$open_time = strtotime($start_time);
$close_time = strtotime($end_time);
$output = date("g:i:a",$open_time);
$output1 = date("g:i:a",$close_time);
?>
<?php
if ($day == 2) {echo $output . "-" . $output1 . '</br>';
}
?> 
<?php endfor; ?><?php endif; ?>         

<?php 

$result2 = queryMysql ("SELECT * FROM doctors WHERE DID='$user_name' AND HID='$HID' AND day=3 ORDER BY start_time ASC ");
$rows2 = mysql_num_rows($result2);
if ($rows2>0):
  echo "Thu:</br>"; 
for($j=0;$j<$rows2;$j++):
  $fee=$start_time=$end_time=$day='';
$row2 = mysql_fetch_array($result2);
$start_time= $row2['start_time'];
$end_time= $row2['end_time'];
$day= $row2['day'];
$open_time = strtotime($start_time);
$close_time = strtotime($end_time);
$output = date("g:i:a",$open_time);
$output1 = date("g:i:a",$close_time);
?>
<?php
if ($day == 3) {echo $output . "-" . $output1 . '</br>';
}
?> 
<?php endfor; ?><?php endif; ?>
<?php 

$result2 = queryMysql ("SELECT * FROM doctors WHERE DID='$user_name' AND HID='$HID' AND day=4 ORDER BY start_time ASC ");
$rows2 = mysql_num_rows($result2);
if ($rows2>0):
  echo "Fri:</br>"; 
for($j=0;$j<$rows2;$j++):
  $fee=$start_time=$end_time=$day='';
$row2 = mysql_fetch_array($result2);
$start_time= $row2['start_time'];
$end_time= $row2['end_time'];
$day= $row2['day'];
$open_time = strtotime($start_time);
$close_time = strtotime($end_time);
$output = date("g:i:a",$open_time);
$output1 = date("g:i:a",$close_time);
?>
<?php
if ($day == 4) {echo $output . "-" . $output1 . '</br>';
}
?> 
<?php endfor; ?><?php endif; ?>          
<?php 

$result2 = queryMysql ("SELECT * FROM doctors WHERE DID='$user_name' AND HID='$HID' AND day=5 ORDER BY start_time ASC ");
$rows2 = mysql_num_rows($result2);
if ($rows2>0):
  echo "Sat:</br>"; 
for($j=0;$j<$rows2;$j++):
  $fee=$start_time=$end_time=$day='';
$row2 = mysql_fetch_array($result2);
$start_time= $row2['start_time'];
$end_time= $row2['end_time'];
$day= $row2['day'];
$open_time = strtotime($start_time);
$close_time = strtotime($end_time);
$output = date("g:i:a",$open_time);
$output1 = date("g:i:a",$close_time);
?>
<?php
if ($day == 5) {echo $output . "-" . $output1 . '</br>';
}
?> 
<?php endfor; ?><?php endif; ?>
<?php 

$result2 = queryMysql ("SELECT * FROM doctors WHERE DID='$user_name' AND HID='$HID' AND day=6 ORDER BY start_time ASC ");
$rows2 = mysql_num_rows($result2);
if ($rows2>0):
  echo "Sun:</br>"; 
for($j=0;$j<$rows2;$j++):
  $fee=$start_time=$end_time=$day='';
$row2 = mysql_fetch_array($result2);
$start_time= $row2['start_time'];
$end_time= $row2['end_time'];
$day= $row2['day'];
$open_time = strtotime($start_time);
$close_time = strtotime($end_time);
$output = date("g:i:a",$open_time);
$output1 = date("g:i:a",$close_time);
?>
<?php
if ($day == 6) {echo $output . "-" . $output1 . '</br>';
}
?> 
<?php endfor; ?><?php endif; ?> 
</div>   
</div> 

<div  class= <?php echo "'datepicker" . $i . "'"; ?> style='display:inline;width:300px;margin-left:3px;'></div>
<div class="slots">    

  <div id='txt<?php echo $i?>' style='display:inline; '> <div class='sampleSlots' style=" margin-top:20px" align='left'>Please select a date to get slots</div></div> 
</div>
</div>
<?php endfor; ?>

<?php die("</body></html>");
endif;
?>    <div id='result' >


<?php

if(isset($_GET['submit2']))
  :  if(isset($_GET['name']))
{
  $name = $_GET['name'];
  $query = " select DISTINCT DID,name,fee,dep,tel,qual,yoe,gender,day FROM doctors_info where doc_name LIKE '%$name%' ";
}
if(isset($_GET['special']))
{
  $dep=$_GET['special'];
  $query .= "AND dep LIKE '%$dep%'";
}
if(isset($_GET['add']))
{
  $add=$_GET['add'];
  $query .= "AND address LIKE '%$add%'";
}
if(isset($_GET['fee']))
{
 $fee=$_GET['fee'];
 $query.=" and fee <= $fee ";
}


$result = mysql_query($query);
$count = mysql_num_rows($result);
if($count>0)
  :

while($row = mysql_fetch_array($result))
  :
$data = $name1 = $tel=$dep=$yoe=$gen=$DID='';
$name1= $row['name'];
$tel= $row['tel'] ;
$dep= $row['dep'] ;
$yoe= $row['yoe'];
$gen= $row['gender'];
$fee=$row['fee'];
$DID= $row['DID'];?>
<div class='appointmentc'>
  <div class= 'appointmentpic'><img src='images/<?php echo $tel?>.jpg' style='width:84px; height: 84px; border-radius:4px;'></img></div>
  <div class='appointmentdetails'> 
   <div class='appointmentname1' >Dr.<?php echo ucfirst($name1)?></div>
   <div class='appointmentname' style='margin-top:12px;'><?php echo $dep?></div> 
   <div class='appointmentname' style='margin-top:28px;'>Experience :&nbsp<?php echo $yoe?> years </div>
   <div class='appointmentname' style='margin-top:44px;'>Rs.<?php echo $fee?>  </div>
 </div>
 <div style="width:0px;position: relative;left: 71%;"><a href='userpage.php?view=<?php echo $tel?>' style='text-decoration:none; color: #fff;'><div class='info' >Info</div></a></div>
 <div style="width:60px;position: relative; left: 84%; top: -19%;"><a href='userpage.php?appointment=<?php echo $DID?>' style='text-decoration:none;color: #4e5665;'>
 <div class='info2' style='padding:9px; margin-top:-10px; margin-left:-10px;'>Appointment</div></a></div>

</div>
<?php endwhile; endif; ?>

<?php
if ($count == 0) {

  echo "<div class='appointmentc' style='margin-left:10px;padding:10px;'>" . 'no data found according to your search!!' . "</div>";
}
?>  
<?php
endif;
if(isset($_GET['submit']))
{
 $con= mysql_connect('localhost','orfom','awerty56');
 mysql_select_db('orfom',$con);
 $noh='';
 $place='';
 if(isset($_GET['name']))
 {
  $noh=$_GET['name'];

}
$query = " select DISTINCT HID,NOH,PlaceOH FROM hospital_info where NOH LIKE '%$noh%' ";

if(isset($_GET['location']))
{
  $place=$_GET['location'];
  $query .= "AND PlaceOH LIKE '%$place%'";
}


$result = mysql_query($query);
$count = mysql_num_rows($result);
if($count>0)
  :
while($row = mysql_fetch_array($result))
  :
$data = $noh = $place=$HID='';
$noh= $row['NOH'];
$place=$row['PlaceOH'];
$HID= $row['HID'];?>
<div class='appointmentc'>
  <div class= 'appointmentpic'></div>
  <div class='appointmentdetails'> 
   <div class='appointmentname1' style='margin-top;20px'/><?php echo ucfirst($noh)?></div>
   <div class='appointmentname' style='width:  21%;margin-top:28px;'>Address : &nbsp<?php echo $place?> </div>
   <div style="width:0px;position: relative;left: 64.5%;top:23.5%;"><a href='userpage.php?viewinfo=<?php echo $HID;?>' style='text-decoration:none; color: #fff;'>
   <div class='info' >Details</div></a></div>
   <div style="width:60px;position: relative; left: 69%;top:22% ;"><a href='userpage.php?viewdoctor=<?php echo $HID?>' style='text-decoration:none;color: #4e5665;'>
   <div class='info2' style='padding:9px;width: 90px;'> View Doctors</div></a></div>


 </div>

</div>
<?php endwhile; endif; ?>

<?php
if ($count == 0) {
  echo "<div class='appointmentc' style='margin-left:10px;padding:10px; >" . 'no data found according to your search!!' . "</div>";
}

}


?>
<?php    
if(! isset($_GET['submit']))

  :
if(!isset($_GET['submit2']))
{
  $doc_limit=5;
  $qry="Select Distinct DID from doctors_info";
  $reslt = mysql_query($qry);
  $nr = mysql_num_rows($reslt);

  if( isset($_GET{'page'} ) )
  {
   $page = $_GET{'page'} + 1;
   $offset = $doc_limit * $page ;
 }
 else
 {
  $page = 0;
  $offset = 0;
}
$left_doc = $nr - ($page * $doc_limit);





$c=array();

$query = "SELECT DISTINCT DID,day,dep,name,tel,qual,yoe,gender,fee FROM doctors_info "."LIMIT $offset, $doc_limit "; 
$result = mysql_query($query);
if (!$result) {die ("Database access failed: " . mysql_error());} 
$rows = mysql_num_rows($result);

$j=1;
while(    $row1 = mysql_fetch_row($result))
 {foreach($row1 as $val1)
   { if(($j-1)%9==0)
    {$DID=$val1;
    }
    elseif(($j-2)%9==0)
      {$c=json_decode($val1);
      }
      elseif(($j-3)%9==0)
        {$dep=$val1;
        }
        elseif(($j-4)%9==0)
          {$name1=$val1;
          }
          elseif(($j-5)%9==0)
            {$tel=$val1;
            }
            elseif(($j-6)%9==0)
              {$qal=$val1;
              }
              elseif(($j-7)%9==0)
                {$yoe=$val1;
                }
                elseif(($j-8)%9==0)
                  {$gender=$val1;
                  }
                  elseif($j%9==0)
                    {$fee=$val1;
                    }
                    $j++;
                    $len=count($c);
                  }
                  ?>

                  <div class='appointmentc' style='height:120px'/>
                  <div class= 'appointmentpic'><img src='doc_image/<?php echo $tel?>.jpg' style='width:85px; height: 85px; border-radius:4px;'/></img></div>
                  <div class='appointmentdetails'> 
                   <div class='appointmentname1' >Dr.<?php echo  ucfirst($name1)?></div>
                   <div class='appointmentname' style='margin-top:12px;'><?php echo ucfirst($dep)?></div> 
                   <div class='appointmentname' style='margin-top:28px;'>Experience :&nbsp<?php echo $yoe?> years </div>
                   <div class='appointmentname' style='margin-top:48px;'>Rs. <?php echo $fee?>  </div>
                   <div class='appointmentname' style='margin-top:66px;'><?php for($i=0;$i<$len;$i++) {echo $c[$i]." "  ;}?>  </div>
                 </div>
                 <div style="width:0px;position: relative;left: 71%;"><a href='userpage.php?view=<?php echo $tel?>' style='text-decoration:none; color: #fff;'><div class='info' >Info</div></a></div>
                 <div style="width:60px;position: relative; left: 84%; top: -19%;"><a href='userpage.php?appointment=<?php echo $DID?>' style='text-decoration:none;color: #4e5665;'>
                 <div class='info2' style='padding:9px; margin-top:-10px; margin-left:-10px;'>Appointment</div></a></div>

               </div>
               <?php }

               if( $page > 0 && ($left_doc > $doc_limit) )
               {
                 $last = $page - 2;
                 echo "<a href=\"userpage.php?page=$last\">  </a> |";
                 echo "<a href=\"userpage.php?page=$page\"> <img src='img/Next-icon.png'width=50px height=50px style='margin-left:5000px'/> </a>";
               }
               else if( $page == 0 )
               {
                 echo "<a href=\"userpage.php?page=$page\"> <img src='img/Next-icon.png'width=50px height=50px style='margin-left:500px'/> </a>";
               }
               else if( $left_doc < $doc_limit )
               {
                 $last = $page - 2;
                 echo "<a href=\"userpage.php?page=$last\"><img src='img/Previous (1).png'width=50px height=50px style='margin-left:450px'/></a>";
               }

               ?>
               <?php } endif; 

               ?>
             </div>


             <div class='sidecontainer'style='background:#3b988e'/>
             <div class='searchcontainer'>
               <input   class="tab1" type="button" name="doc" value="Doctor"onclick="doctor()"style='margin-left:22px'/>
               <input  class="tab1" type="button"  name="hos" value="Hospital" onclick="search()">
               <div id="searcharea">
                <?php if(isset($_GET['submit']))
                { ?>
                <span class="label" style='color: #e9eaed'> Hospital Name </span> <input type='text'id='hospital' name='name' placeholder='eg:AIIMS'value='<?php if(isset($_GET['name'])) echo $_GET['name']; ?>' >
                <span class="label" style='color: #e9eaed'> Location</span> <input type='text' name='location'id='location' placeholder='eg:Hauz Khas' value='<?php if (isset($_GET['location'])) echo $_GET['location']; ?>'> 
                <button onclick='hossearch()'style='margin-left:20px;margin-top:20px;width:100px;height:auto;color:#726486;'>Search</button>
                <?php } else{?>

                <span class="label" style="position: relative;top: 13px;color:#e9eaed;font-family: Helvetica, Arial, 'lucida grande',tahoma,verdana,arial,sans-serif;margin-left: 21px;"> Doctor Name</span>
                 <input type="text" id='autocomplete'name="name" placeholder="eg:Mukesh" value='<?php if(isset($_GET['name'])) echo $_GET['name'];?>' />
                <span class="label" style="position: relative;top: 13px;color: #e9eaed;font-family: Helvetica, Arial, 'lucida grande',tahoma,verdana,arial,sans-serif;margin-left: 21px;">Speciality</span> 
                <input type="text" id='autocomplete2' name="special" placeholder="eg:Neurologist" value='<?php if(isset($_GET['special'])) echo $_GET['special'];?>'/>
                <span class="label" style="position: relative;top: 13px;color: #e9eaed;font-family: Helvetica, Arial, 'lucida grande',tahoma,verdana,arial,sans-serif;margin-left: 21px;">Qualification</span>
                <input type="text" id='qual' name="qual" placeholder="eg:MBBS" />
                <span class="label" style="position: relative;top: 13px;color: #e9eaed;font-family: Helvetica, Arial, 'lucida grande',tahoma,verdana,arial,sans-serif;margin-left: 21px;">Location</span>
                <input type="text" id='add' name='add' placeholder='eg:Hauz Khas' value='<?php if(isset($_GET['add'])) echo $_GET['add']; ?>'/>
                <span class ='any' style='color:#e9eaed;margin-left: 13px;position: relative;top: 20px;'/>
                <input type='checkbox' name='any' value='any' id='any_day' checked  style="left:8px">
                <label class="daybox"for='any_day'  role='button' >ANY</label>  
              </span>
              <br/>
              <span class="day" style=" margin-left:66px;width:30px; height:30px; ">
                <input type="checkbox" name="day[]" value="0" id="monday" style="
                left: -1px;
                ">
                <label class="daybox" style="left: -21px;" for="monday" role="button">M</label>
                <input type="checkbox" name="day[]" value="1" id="tuesday" style="
                left: -9px;
                ">
                <label class="daybox" style="left: -27px;" for="tuesday" role="button">T</label>
                <input type="checkbox" name="day[]" value="2" id="wednesday" style="
                left: -14px;
                ">
                <label class="daybox" style="left: -35px;" for="wednesday" role="button">W</label>
                <input type="checkbox" name="day[]" value="3" id="thursday" style="
                left: -20px;
                ">
                <label class="daybox" style="left: -43px;" for="thursday" role="button">Th</label>
                <input type="checkbox" name="day[]" value="4" id="friday" style="
                left: -32px;
                ">
                <label class="daybox" style="left: -50px;" for="friday" role="button">F</label>
                <input type="checkbox" name="day[]" value="5" id="saturday" style="
                left: -37px;
                ">
                <label class="daybox" style="left: -54px;" for="saturday" role="button">S</label>
                <input type="checkbox" name="day[]" value="6" id="sunday" style="
                left: -42px;
                ">
                <label class="daybox" style="left: -59px;" for="sunday" role="button">S</label></span>
                <input type='range' id='feetext' name='fee' min='0' max='2500' value='2500' step='100'  style='  width: 248px;
                position: relative;
                left: 22px;
                top: 11px;' onchange='updateTextInput(this.value);'/> 
                <label style='position:relative;color:#000;
                left: -230px;
                top: 25px;'/>0 -</label>
                <input style="border: none;
                position: relative;
                background-color: #3B988E;
                left: 47px;
                top: 6px;width:63px; font-size:14px" id='textInput' value='2500' >

                <button onclick='doctorsearch()' style='width:100px;height:auto;color:#726486;position: relative;top: 30px;left: -45px;' >Search </button>
                <?php }?>
              </div>
            </div>
</body>
</html>