<html>
<head>
<link rel="stylesheet" type="text/css" href="css/style.css">

</head>
<body >

<div class="centreimage"></div>

<?php // Example 21-7: login.php



if (version_compare(PHP_VERSION, '5.3.7', '<')) {
    exit("Sorry, Simple PHP Login does not run on a PHP version smaller than 5.3.7 !");
} else if (version_compare(PHP_VERSION, '5.5.0', '<')) {
    
    require_once("login/user_login/libraries/password_compatibility_library.php");
}

require_once("login/user_login/config/db.php");

require_once("login/user_login/classes/Login.php");

$login = new Login();


if ($login->isUserLoggedIn() == true) {
    include("login/user_login/views/logged_in.php");

} else {
    
    include("login/user_login/views/not_logged_in.php");
}


?>
<div class="cranacc1" ><a href="#" style="text-decoration:none"><div class="cranacc1" >Forgot your password?</div></a></div> 
<div class = "seperator"></div>
<div class="cranacc" ><a href="login/user_login/register.php" style="text-decoration:none"><div class="cranacc" >Create an account</div></a></div> 


<div class="backimage"> <a href="index.php"> <div class="backimage1"> <img src="img/back.png"> Back</div> </div>

</body>
</html>