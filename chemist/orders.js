var selected;
var ht =
    '<tr>'+
        '<td class="nameCell">Medicine</td>'+
        '<td class="qtyCell">Qty</td>'+
        '<td class="priceCell">Details</td>' +
    '</tr>';

document.body.onload = function(){
    loading = document.getElementById("loading");
    document.getElementById("orders").style.height=window.innerHeight-50;
    pres = document.getElementById("pres");
    suginit();
}

document.onclick = function(event){
    if(event.target.className=='order'){
        if(selected) selected.classList.remove('selected');
        selected = event.target;
        selected.classList.add('selected');
        loadOrder();
    }
}


function loadOrder(){
    loading.style.display='block';
    pres.innerHTML = "";
    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function(){
        if(ajax.readyState==4 && ajax.status==200){
            result = JSON.parse(ajax.responseText);
            loading.style.display='none';
            document.getElementById('ordersStatus').value = result.status;
            switch(parseInt(result.type)){
            case 1:
                loadAidOrder(result.data);
                break;
            case 2:
                break;
            case 3:
                loadPresData(result.data);
                break;
            }
            loadFiles(selected.id);
        }
    }
    ajax.open('GET','ordersAjax.php?oid='+selected.id,true);
    ajax.send();
}

function loadFiles(id){
    aj = new XMLHttpRequest();
    aj.onload = function(){
        if(aj.readyState==4 && aj.status==200){
            fs = JSON.parse(aj.responseText);
            for(i=0;i<fs.length;i++){
                el=document.createElement('TR');
                el.innerHTML = '<td colspan="3"><img src="'+fs[i]+'"</td>';
                pres.appendChild(el);
            }
        }
    }
    aj.open('GET','getFiles.php?oid='+id,true);
    aj.send();
}

function loadAidOrder(aid){
    htm = ht;
    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function(){
        if(ajax.readyState==4 && ajax.status==200){
            datas = JSON.parse(ajax.responseText);
            for(var i=0;i<datas.length;i++){
                htm += '<tr>'+
                    '<td class="nameCell">'+document.ids[datas[i][0]]+'</td>'+
                    '<td class="qtyCell">'+datas[i][1]+'</td>'+
                    '<td class="priceCell">Your Price</td>'+
                    '</tr>';
            }
            pres.innerHTML = htm;
        }
    }
    ajax.open('GET','../orderMedicine/viewAppoint/index.php?aid='+aid,true);
    ajax.send();
}

function loadPresData(data){
    htm = ht;
    medicines =JSON.parse(data);
    var mids=[];
    for (mid in medicines){
        mids[mids.length] = mid;
    }
    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function(){
        if(ajax.readyState==4 && ajax.status==200){
            datas = JSON.parse(ajax.responseText);
            for(name in datas){
                htm += '<tr>'+
                    '<td class="nameCell">'+name+'</td>'+
                    '<td class="qtyCell">'+medicines[document.words[name]]+'</td>'+
                    '<td class="priceCell">'+datas[name]+'</td>'+
                    '</tr>';
            }
            pres.innerHTML = htm;
        }
    }
    var form = new FormData();
    form.append('data',JSON.stringify(mids));
    ajax.open('POST','getMIDSDataAjax.php',true);
    ajax.send(form);
}

function changeStatus(el){
    if(!selected){
        return;
    }
    ajax = new XMLHttpRequest();
    ajax.onload = function(){
        if(ajax.readyState==4 && ajax.status==200){
            el.value=ajax.responseText;
        }
    }
    ajax.open('GET','changeOrderStatus.php?oid='+selected.id+'&status='+el.value,true);
    ajax.send();
}
