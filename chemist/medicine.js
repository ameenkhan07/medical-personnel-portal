var tableHeads = ["Medicine", "Details", "Available"];
var tableHead;
var cellWidths = ["30%", "50%", "20%"];
var table,search_input;
var fetchDataAjax,saveDataAjax;
var shownMedicines = [];

document.body.onload = function(){
    table = document.getElementById("medicine_table");
    tableHead=createTableHeads();
    table.innerHTML=tableHead;
    search_input = document.getElementById("search_input");
    suginit();
    search_input.onkeyup = function(event){
        if(event.keyCode==13){
            getMedicineById(document.words[search_input.value]);
        }
    }
}

function getMedicineById(id){
    if(!id){
        return;
    }
    index = shownMedicines.indexOf(id);
    if(index != -1){
        return;
    }
    shownMedicines[shownMedicines.length] = id;

    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function(){
        if(ajax.readyState==4 && ajax.status==200){
            document.responseText = ajax.responseText;
            el = document.createElement('tr');
            el.id = id;
            table.appendChild(el);
            value = "";
            if (ajax.responseText != ""){
                value="checked";
            }
            ht = '<td id="'+id+'">'+search_input.value+'</td><td><input type="text" class="price" value="'+ajax.responseText+'"></td><td>'
                +'<input type="checkbox" '+value+' class="available"></td>';
            el.innerHTML = ht;
        }
    }
    ajax.open("GET",'getMedicineById.php?id='+id);
    ajax.send();
}

function makeInput(text){
    value = '';
    if(text!=''){
        value = 'checked';
    }
    ht = '<td><input type="text" class="price" value="'+text+'"></td><td>'
            +'<input type="checkbox" class="available" '+value+'></td>';
    return ht;
}

function createTableHeads() {
    var html = "<tr>";
    for (var i = 0; i < tableHeads.length; i++) {
        html += '<td class="tableHead" style="width:' + cellWidths[i] + '">' + tableHeads[i] + '</td>';
    }
    html += '</tr>';
    return html;
}

function fetchData(){
    medicine_table.innerHTML='';
    shownMedicines = [];
    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function(){
        if(ajax.readyState==4 && ajax.status==200){
            var items = JSON.parse(ajax.responseText);
            var ht=tableHead;
            for(var i=0;i<items.length;i++){
                item=items[i];
                if(shownMedicines.indexOf(document.words[item.name]) != -1){
                    continue;
                }
                ht += '<tr id="'+item.id+'"><td class="medicine_name">'+item.name+'</td>';
                ht += makeInput(item.data) +'</tr>';
                shownMedicines[shownMedicines.length] = document.words[item.name];
            }
            table.innerHTML=ht;
        }
    }
    ajax.open("GET","fetchDataAjax.php?name="+search_input.value,true);
    ajax.send();
}

function save(){
    items = table.getElementsByTagName('tr');
    res=new Object();
    for(var i=0;i<items.length;i++){
        item = items[i];
        if(!item.id) continue;
        item = items[i];
        value = "";
        if(item.getElementsByClassName('available')[0].checked){
            value = item.getElementsByClassName('price')[0].value;
        }
        res[item.id] = value;
    }
    postData = JSON.stringify(res);
    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function(){
        if(ajax.readyState==4 && ajax.status==200){
            alert(ajax.responseText);
        }
    }
    var form = new FormData();
    form.append("data", postData);
    ajax.open("POST","saveDataAjax.php",true);
    ajax.send(form);
    table.innerHTML = tableHead;
    shownMedicines = [];
    search_input.value = "";
}

