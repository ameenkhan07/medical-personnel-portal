<?php
    session_start() or die("Internal Error");
    if (!(isset($_SESSION['chem_login_status']) AND $_SESSION['chem_login_status'] == 1)){
        header("Location:login/index.php");
        exit();
    }
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Chemist</title>
        <link href="style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <table id="header">
            <tr>
                <td class="navL"><a href="index.php"><img src="chemist.jpeg" width="33px" height="33px"></a></td>
                <td class="navM"><a href="index.php">Orders</a></td>
                <td class="navM"><a href="index.php?medicine=1">Medicines</a></td>
                <td class="navR"><a href="login/index.php?logout"><img src="../images/settings.png"/></a></td>
            </tr>
        </table>

        <?php
        if (isset($_GET["medicine"])) {
            require_once ("medicine.php");
        } else if(isset($_GET['setLocation'])){
            require_once ("setLocation.php");
        }else{
            require_once ("orders.php");
        }
        ?>
    </body>
</html>
