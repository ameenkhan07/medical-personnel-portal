<!DOCTYPE html>
<?php session_start(); ?>
<head>
	<!-- <link rel="stylesheet" type="text/css" href="css/style2.css"> -->
	<link rel="stylesheet" type="text/css" href="css/css/font-awesome.min.css ">
	<link rel="stylesheet" href="checkbox.css" type="text/css">	
	<link rel="stylesheet" href="checkbox.css" type="text/css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> 

	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

	<link href="css/css/bootstrap.min.css" rel="stylesheet">

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

	<link href="calendar/css/ui-darkness/jquery-ui-1.10.4.custom.css" rel="stylesheet">
	<link href="css/bootstrap-timepicker.min.css" rel="stylesheet">

	<script type="text/javascript" src="profile.js"></script>
	<script type="text/javascript" src="js/bootstrap-timepicker.min.js"></script>

	<meta charset="utf-8">
	<style>
		.mod{
			margin-left: 10px;
		}
		html{
			width: 100%;
			height: 100%;  For at least Firefox 
			min-height: 100%;
		}
		body{
			height: 100%
			min-height: 100%;
		}

	</style> 
	<script type="text/javascript" >
		
		function showmodal(){
            $('#clinicsubmit').modal('show');
        }

		function appoint(val){
			var  xhr=new XMLHttpRequest();
			xhr.onreadystatechange=function() {
				if (xhr.readyState==4 && xhr.status==200) {
					document.getElementById("appointment-sidemenu").innerHTML=xhr.responseText;
				}
			}
			xhr.open('GET','appoint.php?hid='+val,true);
			xhr.send();
		}
		function clinic()
		{
			var  xhr=new XMLHttpRequest();
			xhr.onreadystatechange=function() {
				if (xhr.readyState==4 && xhr.status==200) {
					document.getElementById("addclinic").innerHTML=xhr.responseText;
				}
			}
			xhr.open('GET','addclinic.php',true);
			xhr.send();
		}
		function clinicdetail()
		{
			var  xhr=new XMLHttpRequest();
		    var clinic=document.getElementById('clinicname').value;
			var address=document.getElementById('address').value;
			var tel=document.getElementById('tel').value;
			xhr.onreadystatechange=function() {
				if (xhr.readyState==4 && xhr.status==200) {
					document.getElementById("content1").innerHTML=xhr.responseText;
				}
			}
			xhr.open('GET','clinicdetail.php?clinic='+clinic+'&address='+address+'&tel='+tel,true);
			xhr.send();
		}

		function slot()	{
			var  xhr=new XMLHttpRequest();
			xhr.onreadystatechange=function() {
				if (xhr.readyState==4 && xhr.status==200) {
					document.getElementById("updateslot").innerHTML=xhr.responseText;
				}
			}
			xhr.open('GET','hos_slot.php',true);
			xhr.send();
		}
		
		function parseScript(strcode) {
			var scripts = new Array();        
			while(strcode.indexOf("<script") > -1 || strcode.indexOf("</script") > -1) {
				var s = strcode.indexOf("<script");
				var s_e = strcode.indexOf(">", s);
				var e = strcode.indexOf("</script", s);
				var e_e = strcode.indexOf(">", e);
	                // Add to scripts array
	                scripts.push(strcode.substring(s_e+1, e));
	                // Strip from strcode
	                strcode = strcode.substring(0, s) + strcode.substring(e_e+1);
	            }
	            for(var i=0; i<scripts.length; i++) {
	            	try {
	            		eval(scripts[i]);
	            	}
	            	catch(ex) {

	            	}
	            }
	        }
	    
	    $(function(){

	    	var message_status = $("#status");
	    	$("td[contenteditable=true]").blur(function(){
	    		var field_userid = $(this).attr("id") ;
	    		var value = $(this).text() ;
	    		$.post('servicejs.php' , field_userid + "=" + value, function(data){
	    			if(data != '')
	    			{
	    				message_status.show();
	    				message_status.text(data);
	    				setTimeout(function(){message_status.hide()},1000);
	    			}
	    		});
	    	});
	    });	
    </script>
</head>

<body >
	<!-- navigation bar -->
	<div class="navbar navbar-default navbar-fixed-top">
		<?php
			$dbhost  = '127.0.0.1';    // Unlikely to require changing
			$dbname  = 'orfom';       // Modify these...
			$dbuser_name  = 'orfom';   // ...variables according
			$dbpass  = 'awerty56';   // ...to your installation
			mysql_connect($dbhost, $dbuser_name, $dbpass) or die(mysql_error());
			mysql_select_db($dbname) or die(mysql_error());
			include 'function.php';
			if (isset($_SESSION['doc_name']))
			{
				$doc_name  = $_SESSION['doc_name'];
				$loggedin = TRUE;
				$doc_namestr  = " ($doc_name)";
			}
			else $loggedin = FALSE;
			if (!$loggedin) die();
			$subquery = "SELECT * FROM doctors_info WHERE doc_name='$doc_name'";
			$subresult = mysql_query($subquery);
			if (!$subresult) die ("Database access failed: " . mysql_error()); 
			$subrow = mysql_fetch_row($subresult);
			$subquery1 = "SELECT * FROM relation WHERE DID='$subrow[0]'";
			$subresult1 = mysql_query($subquery1);
			if (!$subresult1) die ("Database access failed: " . mysql_error()); 
			$subrow1number = mysql_num_rows($subresult1);
			
			// OLD  NAV
			// echo <<<_end
			// <span class="profilepic">
			// <img src='images/$subrow[8].jpg' style="width:32px; height: 32px;margin-top:8px; border-radius:3px;"/>
			// </span> 
			// <span class="profilename" > Dr. $subrow[1]</span>
			// <a href="dlogout.php" style='margin-left:90%'><img src="images/settings2.png" style="border-radius:3px;" > </a> 
			// _end;
		?>
		<!-- NEW SIDEBAR-->
	    <div class="navbar-header">
	    	<a class="navbar-brand  mod" href="docpage1.php"><i class="fa fa-stethoscope"></i> TRANJACT</a> 
	   	</div> 
	</div>
	<!-- ./navigation-bar -->

<!-- main page body -->
	<div class="container col-lg-12" style='margin-top: 7%'>

		<!-- sidebar-->
		<!-- jquery for accordian||SELECT HOSPITAL->LIST->INFORMATION -->
			<div  class="container col-lg-2" >
				
				<!-- <a href="" ><i="" class="fa fa-arrow-left"></a>
				<a href="" ><i="" class="fa fa-arrow-right"></a> -->
				
				<script type="text/javascript" >

					function sideclinic(){
						document.getElementById('clinic-sidemenu').style.display="block";
						document.getElementById('sidemenu').style.display="none";
					}

					function sideclinic2(){
						document.getElementById('appointment-sidemenu').style.display="none";
						document.getElementById('clinic-sidemenu').style.display="block";

					}


					function sideappoint(){
						document.getElementById('clinic-sidemenu').style.display="none";
						document.getElementById('sidemenu').style.display="none";
						document.getElementById('appointment-sidemenu').style.display="block";

					}

					function sidemenu1(){
						document.getElementById('appointment-sidemenu').style.display="none";
						document.getElementById('sidemenu').style.display="block";
					}

					function sidemenu2(){
						document.getElementById('clinic-sidemenu').style.display="none";
						document.getElementById('sidemenu').style.display="block";

					}

				</script>

				<div id='sidemenu' >
						<!-- TABS SIDEBAR NAVIGATION -->
						<ul class="nav nav-pills nav-stacked">
							<li><a href="#appointments" data-toggle="tab" onclick='sideclinic()'>Appointments</a></li>
							<li  class="active"><a href="#profile" data-toggle="tab">Profile</a></li>
							<li><a href="#takeleave" data-toggle="tab">Take Leave</a></li>
							<li onclick='clinic()'><a href="#addclinic" data-toggle="tab">Add Clinic</a></li>
							<li onclick='slot()'><a href="#updateslot" data-toggle="tab">Update Slot</a></li>
						</ul>
				</div>
				<div id="clinic-sidemenu" style="display:none">
					<div id="sidecrumbs">
					    <ol class="breadcrumb">
					      <li><a href="javascript:sidemenu2()">Menu</a></li>
					      <li class="active">Clinics</li>
					    </ol>
					</div>
					<?php
						if ($loggedin)
						{
							if ($subrow1number>0){ ?> 
								<div class="list-group">
								
								<?php while ($subrow1 = mysql_fetch_array($subresult1)) {  
									$HID=$subrow1['HID'];?>
								    <a class="list-group-item btn" name='hidsub' onclick='appoint(<?php echo $HID; ?>);sideappoint()'>
								    	<span class="badge"><i="" class="fa fa-arrow-right"></span><?php echo $subrow1['NOH']; ?>
								    </a>
								<?php	 
								} ?>
								</div>
							<?php 
							}
						} 
					?>
				</div>
				<div id="appointment-sidemenu" style="display:none">		
				</div>
				
			</div>
			
			
			<!-- TAB CONTENTS SECTIONS -->
			<div class="tab-content col-lg-10 ">

				<!-- APPOINTMENT -->
				<div class="tab-pane" id="appointments">
										
				</div>

				<!-- PROFILE -->
				<div class="tab-pane active " id="profile">
					<script type='text/javascript'>
						$(function(){
							var message_status = $("#status");
							$("td[contenteditable=true]").blur(function(){
								var field_userid = $(this).attr("id") ;
								var value = $(this).text() ;
								$.post('servicejs.php' , field_userid + "=" + value, function(data){
									if(data != '')
									{
										message_status.show();
										message_status.text(data);
										setTimeout(function(){message_status.hide()},1000);
									}
								});
							});
						});
					</script>
					
					<?php
						$result = mysqL_query("SELECT * FROM doctors_info  WHERE did='$subrow[0]'");
						$c=array();
						$name1 = $tel=$dep=$yoe=$gen='';
						$row1 = mysql_fetch_array($result);
						$name1 = $tel=$dep=$yoe=$gen=$DID=$NOH='';
						$name1= $row1['name'];
						$tel= $row1['tel'] ;
						$dep= $row1['dep'] ;
						$yoe= $row1['yoe'];
						$gen= $row1['gender'];
						$DID=$row1['DID'];
						$fee=$row1['fee'];
						$day=$row1['day'];
						$c=json_decode($day);
						$len=count($c);
					?>

					<!-- DOC-PIC-INFO -->
					<div class="container col-lg-10">
						<div  class="container col-lg-4" id="docpic" >
							<?php
							if (file_exists("images/$tel.jpg"))
								echo "<img src='images/$tel.jpg' height=250px width=220px/>";    
							?>
						</div>

						<div class="container col-lg-4"  style="margin-top:3%" id="docinfo">
							<?php		
							echo "<h4><b>Dr.".ucfirst($name1)  ."</b></h4>";
							echo "<h5><b>".ucfirst($dep)."</b></h5>";  
							echo "<h5>"."Experience:&nbsp"."$yoe"."&nbsp years"."</h5>";      
							echo "<h5>"."Telephone Number: &nbsp". $tel."</h5>";   
							for($i=0;$i<$len;$i++) echo "<span class=\"badge\">".$c[$i]."</span> ";							
							?>
						</div>
					</div>
					<!-- ./CONTAINER-DOC-PIC-INFOo -->
					
					<!-- .SERVICES -->
					<div class="container col-lg-8">
						<!-- <img src="health-medical-blue.png" title="services" width=40px height=40px /> -->
						<h4><b>Services</b></h4>
						<?php 
							$result1=queryMysql("Select ID, service from services where DID=$DID ");
							$i=1;
							$j=1;
							echo "<table class='table'>";
							while( $row2 = mysql_fetch_row($result1))
								{ echo"<tr class='odd' style='width:300px'>";
							echo "<ul>";
							foreach($row2 as $val)
								{ 	 if(($j-1)%2==0)
									{
										echo"<td id='service:$val'contenteditable='true'>";
										echo "<li>";
									}
									else
									{
										echo $val;
										echo "<i type=\"button\"   class=\"fa fa-close\" style=\"position:absolute;right:1%\"></i>";
										echo "</li>";
										echo "</td>";
									}

									$j++;
								}
								echo"</tr>";
							}
							echo "</ul>";
							echo"</table>";
							?>
					</div>
					
					<!-- .SPECIALIZATION -->
					<div class="container col-lg-8">
						<!-- <img src="icon-healthcare.gif" title="specialization" width=40px height=40px /> -->
						<h4><b>Specialization</b></h4>
						<?php 
						$result1=queryMysql("Select ID,special from specializations where DID=$DID ");

						$i=1;
						$j=1;
						echo "<table class='table'>";
						while( $row2 = mysql_fetch_row($result1))
							{ echo"<tr class='odd' style='width:300px'>";
						echo "<ul>";
						foreach($row2 as $val)
							{ 	 if(($j-1)%2==0)
								{
									echo"<td id='specialization:$val'contenteditable='true'>";
									echo "<li>";
								}
								else
								{
									echo $val;
									echo "<i type=\"button\"   class=\"fa fa-close\" style=\"position:absolute;right:1%\"></i>";
									echo "</li>";
									echo "</td>";
								}
								$j++;
							}
							echo"</tr>";
						}
						echo "</ul>";
						echo"</table>";
						?>
					</div>
					<!-- .EDUCATION -->
					<div class="container col-lg-8">
						<!-- <img src="education.png" title="education" width=40px height=40px /> -->
						<h4><b>Education</b></h4>
						<?php 
						$result1=queryMysql("Select ID, edu from education where DID=$DID ");
						$i=1;
						$j=1;
						echo "<table class='table'>";
						while( $row2 = mysql_fetch_row($result1))
							{ echo"<tr class='odd' >";
						echo "<ul>";
						foreach($row2 as $val)
							{ 	 if(($j-1)%2==0)
								{
									echo"<td id='education:$val'contenteditable='true'>";
									echo "<li>";
								}
								else
								{
									echo $val;
									echo "<i type=\"button\"   class=\"fa fa-close\" style=\"position:absolute;right:1%\"></i>";
									echo "</li>";
									echo "</td>";
								}
								$j++;
							}
							echo"</tr>";
						}
						echo "</ul>";
						echo"</table>";
						?>
					</div>
					<!-- .ACHIEVMENTS -->
					<div class="container col-lg-8">
						<!-- <img src="Awards-324x229-icon.jpg" title="awards" width=60px height=50px /> -->
						<h4><b>Achievments</b></h4>
						<?php 
						$result1=queryMysql("Select ID,award from awards where DID=$DID ");
						$i=1;
						$j=1;
						echo "<table class='table'>";
						while( $row2 = mysql_fetch_row($result1))
							{ echo"<tr class='odd' >";
						echo "<ul>";
						foreach($row2 as $val)
							{ 	 
								if(($j-1)%2==0)
								{
									echo"<td id='award:$val'contenteditable='true'>";
									echo "<li>";
								}
								else
								{
									echo $val;
									echo "<i type=\"button\"   class=\"fa fa-close\" style=\"position:absolute;right:1%\"></i>";
									echo "</li>";
									echo "</td>";
								}
								$j++;
							}
							echo"</tr>";
						}
						echo "</ul>";
						echo"</table>";



						?>
					</div>

					<!-- <div style="position:absolute;margin-top:-70%;width:300px;height:300px;background:#3b988e;">
					
						<input type="hidden" id="did" value="<?php echo $DID;?>">
						
						<input type="text" id="service" name="service" placeholder="Enter Service  "style='width:250px;margin-top:30px'/>
						<button  onclick="service()" style="margin-left:42px;margin-top:10px">Add Services</button>

						<input type="text" id="special" name="specialization" placeholder="Enter Specialization "style='width:250px;'/>
						<button  onclick="specialization()"style="margin-left:42px;margin-top:10px">Add Specialization</button>

						<input type="text" id="edu" name="education" placeholder="Enter Education  "style='width:250px;'/>
						<button  onclick="edu()"style="margin-left:42px;margin-top:10px">Add Education</button>

						<input type="text" id="award" name="award" placeholder="Enter Awards & Recognitions"style='width:250px;'/>
						<button  onclick="award()" style="margin-left:42px;margin-top:10px">Add Awards and Recognition</button>
				    
				    </div>	 -->
				</div>

				<!-- LEAVES -->
				<div class="tab-pane" id="takeleave">
					<div class="container col-lg-12 ">
						<div class="col-lg-5">
							<form  action="leave.php" class="form-horizontal" id="selectleave" role="form">
								
								<div class="form-group" >
							        <input type='hidden' name='did' value='<?php echo $subrow[0]; ?>'>
								</div>

								<div class="form-group">
									<div class="col-lg-10" >
										<input class="form-control text-center" type='date'  id='leave' name='leave'/>
									</div>

								</div>

								<div class="form-group">
									<div class="col-lg-10 ">
								        FROM							
							        <input id="timepicker1" class="form-control text-center" type="text" name='from' value='06:00'>
							    	</div>
							        <script type="text/javascript">
							            $('#timepicker1').timepicker();
							        </script>
									
								</div>
								<div class="form-group">
									<div class="col-lg-10 ">
								        TO							
							        <input id="timepicker2" class="form-control text-center" type="text" name='to' value='10:00:pm'>
							    	</div>
							        <script type="text/javascript">
							            $('#timepicker2').timepicker();
							        </script>
									
								</div>
								<div class="form-group col-lg-10">
									<div class="text-center">
										<button  class="btn btn-primary text-center" 
									type='submit' name="set" value="Set Leave">SET LEAVE</button>
									</div>							
							    </div>
							</form>
						</div>

						<div class="col-lg-5 ol-lg-offset-1">
							<!-- <div id="pickdate" > </div> -->
							<h3 class="text-center">LEAVES</h3>
							<div class="container">
								<?php 
					    			$leavedate = mysql_query("SELECT date,Start_time as start,End_time as end FROM doctor_leave where did=$subrow[0]");
					    		?>	
					    		
					    		<?php	while ($result = mysql_fetch_array($leavedate))
					    			{ ?>
									<table>
					    				<thead>
					    					<tr>
					    						<th>DATE</th>
					    						<th>BETWEEN</th>
					    						<th>DELETE</th>
					    						<tbody>
					    							<tr>
					    								<td >
					    									<div class="col-lg-2">
						   										<?php  echo $result['date'] ;?>
					    									</div>
					    								</td>
					    								<td>
															<div class="col-lg-2 ">
q1<?php  echo $result['start']."-".$result['end'] ;?>
															</div>
					    								</td>
					    								<td>
					    									<div class="col-lg-2">
					    										<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>		
					    									</div>
					    								</td>
					    							</tr>
					    						</tbody>
					    					</tr>
					    				</thead>
					    			</table>
					    		<?php	

					    			}

								 ?>
								<div>
									
								</div>
							</div>

						</div>	
					</div>
					

					<?php
					    $c=array();
					    $leavedate = mysql_query("SELECT date FROM doctor_leave where did=$subrow[0]");
					    while ($result = mysql_fetch_array($leavedate)){
					        $dates[] =  $result['date'];
					    }
					    $c=json_encode($dates);
					?>

					<script type='text/javascript' id='runscript'>

					//     $(function() {
					//         $( "#pickdate" ).datepicker({
					//             dateFormat: 'yy-mm-dd',
					//             beforeShowDay: checkAvailability
					//         });

					//     })
					//     var $myBadDates = <?php echo $c; ?>;
					//     function checkAvailability(mydate){
					//         var $return=true;
					//         var $returnclass ="available";
					//         $checkdate = $.datepicker.formatDate('yy-mm-dd', mydate);
					//         for(var i = 0; i < $myBadDates.length; i++)
					//         {    
					//             if($myBadDates[i] == $checkdate)
					//             {
					//                 $return = false;
					//                 $returnclass= "unavailable";
					//             }
					//         }
					//         return [$return,$returnclass];
					//     }
					// </script>
				</div>

				<!-- ADD CLINIC -->
				<div class="tab-pane" id="addclinic">
					<!-- COMMUNICATES WITH addclinic.php FROM clinic() FUNCTION 	 -->
				</div>

				<!-- UPDATE SLOT -->
				<div class="tab-pane" id="updateslot">
					<!-- COMMUNICATES WITH hos_slot.php FROM slot() FUNCTION 	 -->
				</div>

				<!-- TODO : SUBSTITUTE FOR CONTENT1 WHEREVER POSSIBLE -->
				<!-- <div id='content1' class='container' style='visibility:visible;'></div> -->
			
			</div>

	</div>
	<!-- main-body-close -->


	<!-- Modal -->
	<div class="modal fade" id="clinicsubmit" tabindex="-1" role="dialog" aria-hidden="true">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title" id="myModalLabel">New Clinic Submission</h4>
	            </div>
	            <div class="modal-body">
	                <h2>SUCCESSFULL</h2>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                <button type="button" class="btn btn-primary">Save changes</button>
	            </div>
	        </div>
	    </div>
	</div>

</body>
</html>