<?php
if(!isset($_GET['aid'])){
    die("No aid recieved");
}

$aid = $_GET['aid'];
$path = join('/',str_split(strval($aid)));
$paths = array("uploads/doc/".$path,"uploads/user/".$path,"uploads/patho/".$path);
$res = array('doc'=>array(),'user'=>array(),'patho'=>array());
$types = array('doc','user','patho');
for($i=0;$i<3;$i++){
    if(is_dir($paths[$i])){
        $fs = scandir($paths[$i]);
        $n = count($fs);
        for($j=2;$j<$n;$j++){
            if(is_file($paths[$i].'/'.$fs[$j])) $res[$types[$i]][] = $paths[$i].'/'.$fs[$j];
        }
    }
}
echo json_encode($res);
?>
