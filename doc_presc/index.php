<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>Prescription</title>

		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="js/bootstrap.min.js" rel="javascript">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

		<link rel="stylesheet" href="style.css" type="text/css">
		<script src="code.js" ></script>
		<script src="../chemist/suggestion.js" ></script>
		<script src="../chemist/extrafiles/words.js" ></script>
		<script src="../chemist/extrafiles/ids.js" ></script>
		<script type="text/javascript" src="../patho/extrafiles/patho_list.js"></script>
		<script type="text/javascript" src="../patho/extrafiles/tests.js"></script>
		<script type="text/javascript" src="../patho/extrafiles/testsids.js"></script>
		<script type="text/javascript" src="../patho/extrafiles/testgroupsids.js"></script>
		<script type="text/javascript" src="../patho/extrafiles/testgroups.js"></script>
		<script type="text/javascript" src="../patho/extrafiles/groups.js"></script>
		<script type="text/javascript" src="../patho/suggestion.js"></script>
	</head>

	<body onload="init()">
		<script type="text/javascript">

			document.aid = ""+<?php echo $_POST["aid"]; ?>
	+"";
		</script>

		<div class="header">
    		<?php
            session_start();
            $dbhost  = '127.0.0.1';    // Unlikely to require changing
            $dbname  = 'orfom';       // Modify these...
            $dbuser_name  = 'orfom';   // ...variables according
            $dbpass  = 'awerty56';   // ...to your installation

            mysql_connect($dbhost, $dbuser_name, $dbpass) or die(mysql_error());
            mysql_select_db($dbname) or die(mysql_error());

            include '../function.php';
            if (isset($_SESSION['doc_name']))
            {
                $doc_name  = $_SESSION['doc_name'];
                $loggedin = TRUE;
                $doc_namestr  = " ($doc_name)";
            }

            else $loggedin = FALSE;
            if (!$loggedin) die();
            $subquery = "SELECT * FROM doctors_info WHERE doc_name='$doc_name'";
            $subresult = mysql_query($subquery);
            if (!$subresult) die ("Database access failed: " . mysql_error());
            $subrow = mysql_fetch_row($subresult);

            $subquery1 = "SELECT * FROM relation WHERE DID='$subrow[0]'";
            $subresult1 = mysql_query($subquery1);
            if (!$subresult1) die ("Database access failed: " . mysql_error());
                $subrow1 = mysql_fetch_array($subresult1);

            if ($loggedin and  isset($_POST['hid1']) and  isset($_POST['aid']) and  isset($_POST['uid']) ){

            $hid1=$_POST['hid1'];
            $uid=$_POST['uid'];
            $user_name=$_POST['user_name'];
            $user_tel=$_POST['user_tel'];
            $aid=$_POST['aid'];
            echo
            "<span class='profilepic'>".
                "<img src='../images/$subrow[8].jpg' style='width:32px; height: 32px;'/>".
            "</span>".
            "<span class='profilename'> Dr. $subrow[1]</span>".
            "<span class='profileid'> HID: $hid1</span>
            <a href='../dlogout.php' style='margin-top:10px;margin-left:90%;'><img src='../images/settings2.png' style='border-radius:3px;' ></a>";




    			?>
		</div>
        <!-- NAVBAR -->
        <div class="col-lg-8 col-lg-offset-2">
    		<div id="tabs">
                <ul class="nav nav-tabs">
                <li role="presentation" class="active" id="selectedTabButton"> <a onclick ="changeTab(0,this)" data-toggle="tab">Details</a></li>
                <li role="presentation" > <a onclick ="changeTab(1,this)" data-toggle="tab" >Prescription</a></li>
                <li role="presentation" > <a onclick ="changeTab(2,this)" data-toggle="tab">Files</a></li>
                </ul>   
            </div>
		<div class="tab" id="tab0">
			<div class="container" id="detailContainer" style="text-align:left">
                <div id="patient">
                    <img src="8130833234.jpg" alt="Patient"></img>
                </div>

                <div id="filebox" style="left: 200px;">  
                    <dl class="dl-horizontal" style="font-size: 15px">
                       <div class="col-lg-offset-3"  style="color: green">
                            <h3>Profile</h3>
                        </div>

                        <dt>Name: </dt>
                        <dd> <?php echo"$user_name" ?> </dd></br>
                        <dt>Age: </dt>
                        <dd>20</dd></br>
        			    <dt>Sex: </dt>
                        <dd>Male</dd></br>
                        <dt>Blood Group: </dt>
                        <dd>A+</dd></br>
                </div>

                <div id="filebox" style="left: calc(100% - 400px)">
                    <div class="col-lg-offset-2" style="color: green">
                            <h3>Contact Details</h3>
                        </div>
                    <dl class="dl-horizontal" style="font-size: 15px">
                        <dt>Telephone : </dt>
                        <dd> <?php echo"$user_tel" ?> </dd></br>
                    </dl>
                </div>
            </div>
		</div>
		<div class="tab" id="tab1" style="display:none">
            <form>
                <div class="form-group col-lg-12 prob" >
                    <label for="problems">Problems</label>
                    <textarea type="text" class="form-control " id="problems"></textarea>
                </div>
            </form>
            <div class="container col-lg-12">
                <table class="table table-striped table-bordered prob" id="presTable">
                    <tr>
                        <td class="mediNameCell"><b>Medicine</b></td>
                        <td class="mediQtyCell"><b>Qty</b></td>
                        <td class="mediRemarkCell"><b>Remarks</b></td>
                        <td class="action">Action</td>
                    </tr>
                    <tr>
                        <td class="mediNameCell">
                           <div class="form-group">
                                <input list="words" class="form-control" type="text" placeholder="medicine" id="mediNameInput">
                            </div>
                        </td>
                        <td class="mediQtyCell">
                            <form>
                              <div class="form-group">
                                <input type="text" class="form-control" id="mediQtyInput" placeholder="Qty">
                              </div>
                            </form>
                        </td>
                        <td class="mediRemarkCell">
                            <form>
                              <div class="form-group">
                                <input type="text" class="form-control" id="mediRemarkInput" placeholder="Remarks">
                              </div>
                            </form>
                        </td>
                        <td class="action">
                            <button type="button" style="background: none; border: none; box-shadow: none" onclick="addMedicine()">
                                <span class="glyphicon glyphicon-plus" style="font-size: 1.8em"></span>
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="container col-lg-12">
                <table id="testTable" class="table table-striped table-bordered col-lg-8 prob">
                    <tr>
                        <td class="testNameCell"><b>Test Name</b></td>
                        <td class="testRemarkCell"><b>Remarks</b></td>
                        <td class="action">Action</td>
                    </tr>
                    <tr>
                        <td class="testNameCell">
                            <form class="form-group">
                                <input type="text" class="form-control" id="testNameInput" list="testList" placeholder="Test Name">
                            </form>
                        </td>
                        
                        <td class="testRemarkCell">
                            <form>
                              <div class="form-group">
                                <input type="text" class="form-control" id="testRemarkInput" placeholder="Remarks">
                              </div>
                            </form>
                        </td>
                        <td class="action col-lg-offset-2">
                            <button type="button" style="background: none; border: none; box-shadow: none" onclick="addTest()">
                                <span class="glyphicon glyphicon-plus" style="font-size:1.8em"></span>
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
            <form>
                <div class="form-group col-lg-12 prob">
                    <label for="problems">Final Report</label>
                    <textarea type="text" class="form-control " id="report"></textarea>
                </div>
            </form>
            <div class="container col-lg-12">
                <button type="button" class="btn btn-primary prob" onclick ="save()">Save</button>
            </div>
        </div>
		<div class="tab" id="tab3" style="display:none">
            <div id="fileContainer"> 
                <div id="sideNav" role="navigation">
                    <ul class="nav nav-pills nav-stacked" id="fButton">
                        <li role="presentation" class="active" > <a onclick="changeFiles('docfilebox',this)" data-toggle="tab">My files</a></li>
                        <li role="presentation" > <a onclick="changeFiles('userfilebox',this)" data-toggle="tab">Files from user</a></li>
                        <li role="presentation"> <a onclick="changeFiles('pathofilebox',this)" data-toggle="tab">Files from Lab</a></li>
                    </ul>
                </div>
                <div id="filebox" style="left: 20%; width: 800px">
                    <div class="jumbotron">
                        <div id="docfilebox">
                            <?php
                            $imageFileTypes = array('jpg', 'JPG', 'png' ,'PNG' ,'jpeg' ,'JPEG','gif','GIF');
                            $path = "uploads/doc/".join('/',str_split(strval($_POST['aid'])));
                            if(is_dir($path)){
                                $ar = scandir($path);
                                $n = count($ar);
                                for($i=0;$i<$n;$i++){
                                    if(is_file($path.'/'.$ar[$i])) {
                                        $ext = pathinfo(basename($path.'/'.$ar[$i]));
                                        $ext = $ext['extension'];
                                        if(in_array($ext, $imageFileTypes)) {
                                            echo '<img class="image img-thumbnail" style="padding: 10px; margin: 20px" width="128" height="128" src="'.$path.'/'.$ar[$i].'">';
                                        }else{
                                            echo '<a href="'.$path.'/'.$ar[$i].'"><img width="128" class="image img-thumbnail" style="padding: 10px; margin: 20px" height="128" src="../images/fileicon.png"></a>';
                                        }
                                    }
                                }
                            }
                            ?>
                            <img width="128" height="128" src="../images/newfile.png" class="image img-thumbnail" style="padding: 10px; margin: 20px" onclick="showUploadPopup()">
                        </div>
                        <div id="userfilebox" style="display:none">
                            <?php
                            $path = "uploads/user/".join('/',str_split(strval($_POST['aid'])));
                            if(is_dir($path)){
                                $ar = scandir($path);
                                $n = count($ar);
                                for($i=0;$i<$n;$i++){
                                    if(is_file($path.'/'.$ar[$i])) {
                                        $ext = pathinfo(basename($path.'/'.$ar[$i]));
                                        $ext = $ext['extension'];
                                        if(in_array($ext, $imageFileTypes)) {
                                            echo '<img class="image img-thumbnail" style="padding: 10px; margin: 20px" width="128" height="128" src="'.$path.'/'.$ar[$i].'">';
                                        }else{
                                            echo '<a href="'.$path.'/'.$ar[$i].'"><img width="128" class="image img-thumbnail" style="padding: 10px; margin: 20px" height="128" src="../images/fileicon.png"></a>';
                                        }
                                    }
                                }
                            }
                            ?>
                        </div>
                        <div id="pathofilebox" style="display:none">
                            <?php
                            $path = "uploads/patho/".join('/',str_split(strval($_POST['aid'])));
                            if(is_dir($path)){
                                $ar = scandir($path);
                                $n = count($ar);
                                for($i=0;$i<$n;$i++){
                                    if(is_file($path.'/'.$ar[$i])) {
                                       $ext = pathinfo(basename($path.'/'.$ar[$i]));
                                       $ext = $ext['extension'];
                                        if(in_array($ext, $imageFileTypes)) {
                                            echo '<img class="image img-thumbnail" style="padding: 10px; margin: 20px" width="128" height="128" src="'.$path.'/'.$ar[$i].'">';
                                        }else{
                                            echo '<a href="'.$path.'/'.$ar[$i].'"><img width="128" class="image img-thumbnail" style="padding: 10px; margin: 20px" height="128" src="../images/fileicon.png"></a>';
                                        }
                                    }
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div id='fileUploadDiv' style="display:none">
                    <form name="fileForm" enctype="multipart/form-data">
                      <input type="text" id="fileName" placeholder="Name" name="name">
                      <input type="file" id="fileInput" name="upfile" onchange = "setName(this)">
                      <button onclick = "uploadNewFile()">Upload</button>
                      File Upload Progress:
                      <progress id="fileUploadProgress" min="0" max="100" value="0"></progress>
                  </form>
              </div>

            </div>
        </div>
    </div>
        <div class="popup" align="center" id="imageViewPopup"></div>
		<?php } ?>
	</body>

</html>
