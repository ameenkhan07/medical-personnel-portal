var medicineTable,testTable;
var mediNameInput,mediQtyInput,mediRemarkInput;
var testNameInput,testRemarkInput;
var medicines = [],tests=[]; //for storing names of medicines and test which are added in the prescription
var edit,editables=['mediQty','mediRemark','testName','testRemark']; //class name of html elements which can be edited on doubleclick
var tabs,selectedTab,selectedTabButton;
var files,fileType=0,types=['doc','user','patho'];
var imageTypes = ['png','jpg','jpeg'];
var sb, sid = 'docfilebox';
function init(){
    medicineTable = document.getElementById('presTable');
    testTable = document.getElementById('testTable');
    mediNameInput = document.getElementById('mediNameInput');
    mediQtyInput = document.getElementById('mediQtyInput');
    testNameInput = document.getElementById('testNameInput');
    testRemarkInput = document.getElementById('testRemarkInput');
    mediRemarkInput = document.getElementById('mediRemarkInput');
    fetchData();
    suginit();
    testsuginit();
    tabInit();
    sb = document.getElementById('fButton');

}

function showUploadPopup(){
    document.getElementById('fileUploadDiv').style.display="block";
}

function changeFiles(id,el){
    if(id == sid) return;
    document.getElementById(id).style.display = 'block';
    document.getElementById(sid).style.display = 'none';
    
    sb = el;
    sid = id;
}



function tabInit(){
    tabs = document.getElementsByClassName("tab");
    selectedTab = document.getElementById("tab0");
    selectedTabButton = document.getElementById("selectedTabButton");
}

function addMedicine(){
    if(medicines.indexOf(mediNameInput.value) != -1){
        return;
    }
    if((mediNameInput.value == '') ||( mediQtyInput =='')){
        return;
    }
    id = document.words[mediNameInput.value];
    if((!id)&&(confirm("Do you want to add new medicine - "+mediNameInput.value))) {
        ajax = new XMLHttpRequest();
        form = new FormData();
        form.append('add','1');
        form.append('medicine_name',mediNameInput.value);
        ajax.open("POST","../chemist/add_medicine.php",false);
        ajax.send(form);
        id = ajax.responseText;
        document.words[mediNameInput.value] = id;
        document.words[id] = mediNameInput.value;
    }
    remark = "Nothing";
    if(mediRemarkInput.value != ""){
        remark = mediRemarkInput.value;
    }
    el = document.createElement('tr');
    el.id = id;
    ht =    '<td class="mediName" style="padding-left: 10px">'+mediNameInput.value+'</td>'+
            '<td class="mediQty" style="padding-left: 10px">'+mediQtyInput.value+'</td>'+
            '<td class="mediRemark" style="padding-left: 10px">'+remark+'</td>'+
            '<td class="action"><img class="deleteMedi"  style="padding-top: 7px; padding-bottom: 7px; padding-left: 12px; padding-right: 3px" src="remove.jpg" el="'+medicines.length+'"></td>';
    el.innerHTML  = ht;
    el.classList.add('dataRow');
    medicineTable.appendChild(el);
    medicines[medicines.length] = mediNameInput.value;

    mediNameInput.value = '';
    mediQtyInput.value = '';
    mediRemarkInput.value = '';
}

function addTest(){
    if(tests.indexOf(testNameInput.value) != -1){
        return;
    }
    if(testNameInput.value == ''){
        return;
    }

    remark = "Nothing";
    if(testRemarkInput.value!=""){
        remark = testRemarkInput.value;
    }

    el = document.createElement('tr');
    el.classList.add('dataRow');
    ht =    '<td class="testName" style="padding-left: 10px;">'+testNameInput.value+'</td>'+
            '<td class="testRemark" style="padding-left: 10px;">'+remark+'</td>'+
            '<td class="action"><img class="deleteTest"  style="padding-top: 7px; padding-bottom: 7px; padding-left: 10px; padding-right: 4px" src="remove.jpg"el="'+tests.length+'"></td>';
    el.innerHTML  = ht;
    testTable.appendChild(el);
    tests[tests.length] = testNameInput.value;
    testNameInput.value = '';
    testRemarkInput.value = '';
}

document.onclick = function(event){
    if(event.target.classList.contains('image')){
        el = document.getElementById('imageViewPopup');
        el.innerHTML = '<img src="'+event.target.src+'">';
        el.style.display='block'; 
        el.style.animation = 'zoom 0.5s';
        el.style.width='100%';
        el.style.height='100%';
    }else if(event.target.classList.contains('popup')) {
        el = event.target;
        el.style.animation = 'fade 0.5s';
        el.style.width='0';
        el.style.height='0';
    }

    if(event.target.className=='deleteMedi'){
        event.target.parentNode.parentNode.remove();
        medicines.splice(parseInt(event.target.el),1);
    }else if(event.target.className=='deleteTest'){
        event.target.parentNode.parentNode.remove();
        tests.splice(parseInt(event.target.el),1);
    }else if(editables.indexOf(event.target.className) != -1){
        event.target.contentEditable=true;
        if(edit){
             edit.contentEditable = false;
             edit.classList.remove('editable');
         }
        edit = event.target;
        edit.classList.add('editable');
    }else{
        if(!edit) return;
        if(event.target == edit){
            return;
        }
        edit.contentEditable = false;
        edit.classList.remove('editable');
    }
}

document.onkeyup = function(event){
    if(event.keyCode==13){
        if(event.target == mediRemarkInput) {
            addMedicine();
        }else if(event.target==testRemarkInput){
            addTest();
        }
    }if(event.keyCode==27){
        document.getElementById('imageViewPopup');
        if(el.style.display=='block'){
            el.style.animation = 'fade 0.5s';
            el.style.width='0';
            el.style.height='0';
        }
    }
}

document.onkeydown = function (event){
    if((event.keyCode==13)&&(event.target == edit)) {
            edit.contentEditable = false;
            edit.classList.remove('editable');
    }
}

function save(){
    medis = [];
    rows = medicineTable.getElementsByClassName('dataRow');
    for(var i=0;i<rows.length;i++){
        medi = [rows[i].id,0,0];
        for(var j=0;j<rows[i].children.length;j++){
            switch(rows[i].children[j].className){
            case 'mediQty':
                medi[1] = rows[i].children[j].innerHTML;
                break;
            case 'mediRemark':
                medi[2] = rows[i].children[j].innerHTML;
                break;
            }
        }
        medis[medis.length] = medi;
    }

    testsData = [];
    rows = testTable.getElementsByClassName('dataRow');
    for(var i=0;i<rows.length;i++){
        test = [0,0];
        for(var j=0;j<rows[i].children.length;j++){
            switch(rows[i].children[j].className){
            case 'testName':
                test[0] = rows[i].children[j].innerHTML;
                break;
            case 'testRemark':
                test[1] = rows[i].children[j].innerHTML;
                break;
            }
        }
        testsData[testsData.length] = test;
    }

    form = new FormData();
    form.append('medicines',JSON.stringify(medis));
    form.append('tests',JSON.stringify(testsData));
    form.append('problems',document.getElementById('problems').value)
    form.append('report',document.getElementById('report').value)
    form.append('aid',document.aid);

    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function(){
        if(ajax.readyState==4 && ajax.status==200){
            alert(ajax.responseText);
        }
    }
    ajax.open('POST','saveajax.php',true);
    ajax.send(form);
}

function fetchData(){
    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function(){
        if(ajax.readyState == 4 && ajax.status==200){
            response = JSON.parse(ajax.responseText);
            medis = JSON.parse(response.medicines);
            testsData = JSON.parse(response.tests);
            loadData(medis,testsData);
            document.getElementById('problems').value = response.problems;
            document.getElementById('report').value = response.report;
            document.medis = medis;
        }
    }
    ajax.open('GET','fetchajax.php?aid='+document.aid,true);
    ajax.send();
}

function loadData(medis,testsData){
    for (i in medis){
        mediNameInput.value = document.ids[medis[i][0]];
        mediQtyInput.value = medis[i][1];
        mediRemarkInput.value = medis[i][2];
        addMedicine();
    }
    for (j in testsData){
        testNameInput.value = testsData[j][0];
        testRemarkInput.value = testsData[j][1];
        addTest();
    }
}

function changeTab(tab,el){
    selectedTab.style.display = 'none';
    selectedTab = tabs[tab];
    selectedTab.style.display = 'block';
    
    
    selectedTabButton = el;
    
    ;
}