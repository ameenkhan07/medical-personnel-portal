var fileOptions,seletedFileOption;
var medicineTable,medicineInput,medicines;
var seletedChemist=null,chemistDiv,pincodeInput;
var mapDiv,map=null,geocoder;
var aid=0,chemistList,markers=[];
var selectedChemistId = 0;
var chemistDetailsDiv;
var tabs=['uploadTab','presTab','appointTab'],selectedTab=0;
var hasPresMedi = false;
var exHT =  "<img src=\"exit.jpg\" width=\"40\" height=\"40\" id=\"exitImage\" onclick=\"document.getElementById('chemistDetailsDiv').style.display='none';\">";

function init(){
    fileOptions = document.getElementsByClassName('fileoption');
    medicineTable = document.getElementById('medicines');
    medicineInput = document.getElementById('medicine_input');
    pincodeInput = document.getElementById('pincode');
    mapDiv = document.getElementById('chemistsMap');
    medicines = [];
    seletedFileOption = 0;
    suginit();
    chemistList = JSON.parse(document.rawChemists);
    medicineInput.onkeyup = function(event){
        if(event.keyCode == 13){
            addMedicine(medicineInput.value);
        }
    }
    mapInit();
}

function selectTab(option){
    document.getElementById(tabs[selectedTab]).classList.remove('tab');
    document.getElementById(tabs[selectedTab]).classList.add('hiddenTab');
    selectedTab = option;
    document.getElementById(tabs[selectedTab]).classList.remove('hiddenTab');
    document.getElementById(tabs[selectedTab]).classList.add('tab');
    fileOptions[seletedFileOption].style.display = 'none';
    seletedFileOption = option;
    fileOptions[seletedFileOption].style.display = 'block';
}


function addMedicine(medicine){
    id = document.words[medicine];
    if(!id) return;
    if(medicines.indexOf(id) != -1){
        return;
    }
    el = document.createElement('tr');
    el.id = id;
    el.classList.add("medicineRow");
    medicineTable.appendChild(el);
    ht = '<td class="medicineNameCell">'+medicine+'</td><td '+
            'class="medicinePriceCell">Choose Chemist</td><td '+
            'id="medicineQtyCell"><input type="text"></td><td><img src="delete.png" class="deleteButton" width="20" height="20"></td>';
    el.innerHTML = ht;
    medicines[medicines.length] = id;
    refreshInfo();
}

document.onclick = function (event){
    if(event.target.className=='deleteButton'){
        tr = event.target.parentNode.parentNode;
        medicines.splice(medicines.indexOf(tr.id),1);
        tr.remove();
    }else if(event.target.id == 'popup'){
        document.getElementById('popup').style.display = 'none';
    }
}

function refreshInfo(){
    if(selectedChemistId == 0){
        return;
    }
    data = new FormData();
    data.append('CID',selectedChemistId);
    data.append('medicines',JSON.stringify(medicines));
    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function(){
        if(ajax.readyState==4 && ajax.status==200){
            datas = JSON.parse(ajax.responseText);
            for(var d in datas){
                v = 'Not available';
                if(datas[d] != ''){
                    v = datas[d];
                }
                document.getElementById(d).getElementsByClassName('medicinePriceCell')[0].innerHTML = v;
            }
        }
    }
    ajax.open('POST','getMedicineInfo.php');
    ajax.send(data);
}

function changeAid(){
    aid = document.getElementById("aidSelect").value;
    if(aid == ""){
        return;
    }
    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function(){
        if(ajax.readyState==4 && ajax.status==200){
            if(ajax.responseText==''){
                document.getElementById('aidDetails').innerHTML='<tr><td class="medicineNameCell">Medicine</td><td class="medicineQtyCell">Qty</td><td class="medicinePriceCell">Price/Details</td></tr>';
                return;
            }
            data = JSON.parse(ajax.responseText);
            medicines = [];
            ht = '<tr><td class="medicineNameCell">Medicine</td><td class="medicineQtyCell">Qty</td><td class="medicinePriceCell">Price/Details</td></tr>';
            for(i=0;i<data.length;i++){
                medicines[medicines.length] = data[i][0];
                ht += '<tr id="'+data[i][0]+'"><td class="medicineNameCell">'+document.ids[data[i][0]]+'</td><td class="medicineQtyCell">'+data[i][1]+'</td><td class="medicinePriceCell">Select Chemist</td></tr>';
            }
            document.getElementById('aidDetails').innerHTML=ht;
        }
    }
    ajax.open("GET",'/~praveenkumar/orfom/ordermedicine/viewAppoint?aid='+aid);
    ajax.send();
}

function placeOrder(){
    if(selectedChemistId == 0){
        alert("Chemist not Selected");
        return;
    }

    if(document.getElementById('shippingAddress').value == ""){
        alert("No Shipping Address");
        return;
    }

    if(document.getElementById('mobile').value == ""){
        alert("No mobile no");
        return;
    }
    if(hasPresMedi){
    els = document.getElementsByClassName('fileInput1');
        for(i=0;i<els.length;i++){
            if(!els[i].value){
                alert("Some files are not selected");
                return;
            }
        }
    }
    var form = document.getElementById("orderForm");
    chemistEl = document.createElement('input');
    chemistEl.setAttribute("type","hidden");
    chemistEl.setAttribute("name","chemist");
    chemistEl.setAttribute("value",selectedChemistId);
    form.appendChild(chemistEl);

    typeEl = document.createElement('input');
    typeEl.setAttribute("type","hidden");
    typeEl.setAttribute("name","type");
    switch(fileOptions[seletedFileOption].id){
    case "aid":
        typeEl.setAttribute("value","1");
        break;
    case "openFile":
        typeEl.setAttribute("value","2");
        els = document.getElementsByClassName("fileInput");
        for(i=0;i<els.length;i++){
            if(! els[i].value){
                alert("Some files not selected");
                return;
            }
        }
        break;
    case "file":
        typeEl.setAttribute("value","3");
        meds = document.getElementsByClassName("medicineRow");
        if(meds.length == 0){
            alert("No medicines");
            return;
        }
        data = new Object();
        for (var i=0;i<meds.length;i++){
            data[meds[i].id] = meds[i].getElementsByTagName('input')[0].value;
        }
        dataEl = document.createElement('input');
        dataEl.setAttribute("type","hidden");
        dataEl.setAttribute("name","medicines");
        dataEl.setAttribute("value",JSON.stringify(data));
        form.appendChild(dataEl);
        break;
    }
    form.appendChild(typeEl);
    form.submit();
}

function showPosition(position){
    map.setCenter(new google.maps.LatLng(position.coords.latitude,position.coords.longitude));
}

function geocodeCallBackC(results,status){
    if(status == google.maps.GeocoderStatus.OK){
         createMap(results[0].geometry.location);
         map.fitBounds(results[0].geometry.viewport);
    }else{
        createMap(null);
    }
}

function createMap(c){
    if(!c) c = { lat: 28.535364427922303, lng: 77.21867267941684};
    var mapOptions = {
        center: c,
        zoom: 11,
        mapTypeControl: false,
        zoomControl:true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL
        }
    };

    map = new google.maps.Map(mapDiv,mapOptions);
    for(var i=0;i<chemistList.length/3;i++){
        var latlng = new google.maps.LatLng(parseFloat(chemistList[3*i+2]), parseFloat(chemistList[3*i+1]));
        markers[markers.length] = new google.maps.Marker({map: map,position: latlng});
        markers[markers.length-1].mydata = chemistList[3*i];
        google.maps.event.addListener(markers[markers.length-1], 'click', markerClicked);
    }
}

function mapInit(){
    geocoder = new google.maps.Geocoder();
    if(document.place){
        pincodeInput.value = document.place;
        geocoder.geocode({'address':document.place},geocodeCallBackC);
        return;
    }
    createMap(null);
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    }
}

function markerClicked(e) {
    selectedChemistId = this.mydata;
    showChemistDetails();
    refreshInfo();
}

function geocodeCallBack(results,status){
    if(status == google.maps.GeocoderStatus.OK){
         map.setCenter(results[0].geometry.location);
         map.fitBounds(results[0].geometry.viewport);
    }else{
        alert('Geocode was not successful for the following reason: ' + status);
    }
}

document.onkeyup = function (event){
    if((event.keyCode == 13)&&(event.target==pincodeInput)){
        var address = pincodeInput.value;
        if(!geocoder) return;
        geocoder.geocode({'address':address},geocodeCallBack);
    }else if(event.keyCode == 27){
        document.getElementById('popup').style = 'block';
    }
}

function showChemistDetails(){
    chemistDetailsDiv =  document.getElementById('chemistDetailsDiv');
    chemistDetailsDiv.innerHTML = exHT+'<img src="loading.gif" class="loadingImage" width="64" height="64">';
    ajax1 = new XMLHttpRequest();
    ajax1.onreadystatechange = function(){
        if(ajax1.readyState==4 && ajax1.status==200){
            data = JSON.parse(ajax1.responseText);
            document.getElementById('chemistNameDiv').innerHTML = data['name'];
            ht = exHT;
            ht += 'Email  '+data['email']+'<br>';
            ht += 'Mobile '+data['tel'] + '<br>';
            ht += 'Address '+data['address'] + '<br>';
            chemistDetailsDiv.innerHTML = ht;
            chemistDetailsDiv.style.display = 'block';
        }
    }
    ajax1.open("GET",'getChemistById.php?id='+selectedChemistId,true);
    ajax1.send();
}

function addAnotherFile(){
    els = document.getElementsByClassName('fileInput');
    for(i=0;i<els.length;i++){
        if(!els[i].value){
            alert("Some files are not selected");
            return;
        }
    }
    el = document.createElement('INPUT');
    el.setAttribute("type","file");
    el.setAttribute("name","upfile[]");
    el.setAttribute("form","orderForm");
    el.setAttribute("class","fileInput");
    fileOptions[0].appendChild(el);
}

function addAnotherFile1(){
    els = document.getElementsByClassName('fileInput1');
    for(i=0;i<els.length;i++){
        if(!els[i].value){
            alert("Some files are not selected");
            return;
        }
    }
    el = document.createElement('INPUT');
    el.setAttribute("type","file");
    el.setAttribute("name","upfile1[]");
    el.setAttribute("form","orderForm");
    el.setAttribute("class","fileInput1");
    document.getElementById('exPresDiv').appendChild(el);
}


function proceed(){
    document.getElementById("popup").style.display = 'block';
    presMedi();
    if(hasPresMedi){
        document.getElementById('exPresDiv').style.display = 'block';
    }
}

function presMedi(){
    if(selectedTab != 1) return;
    aj = new XMLHttpRequest();
    f = new FormData();
    f.append('medis',JSON.stringify(medicines));
    aj.open('POST','isPresMedi.php',false);
    aj.send(f);
    hasPresMedi = (aj.responseText == '200');
}
