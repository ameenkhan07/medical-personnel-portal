<?php

class ChemLogin {

    private $db_connection = null;

    public $error = "";

    public function __construct() {
        session_start();
        if (isset($_GET["logout"])) {
            $this -> doLogout();
        } else if (isset($_POST["login"])) {
            $this -> dologinWithPostData();
        }
    }

    private function dologinWithPostData() {
        // check login form contents

        $username = $_POST["username"];
        $password = $_POST["password"];
        if (empty($_POST['username'])) {
            $this -> errors[] = "Username field was empty.";
        } elseif (empty($_POST['password'])) {
            $this -> errors[] = "Password field was empty.";
        } elseif (!empty($_POST['username']) && !empty($_POST['password'])) {
            $conn = mysql_connect('localhost', 'orfom', 'awerty56');
            if (!$conn) {
                die('internal error');
            } else {
                mysql_select_db('orfom', $conn);
                $query = "SELECT * FROM `patho` WHERE (username='$username')";
                $result = mysql_query($query) or die("Internal Error");
                if ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
                    if (password_verify($password, $row["password_hash"])) {
                        $_SESSION["path_username"] = $row['username'];
                        $_SESSION["PID"] = $row['PID'];
                        $_SESSION["patho_login_status"] = 1;
                        mysql_close($conn);
                        header("Location:../index.php");
                        exit();
                    }
                    $this -> error = "Wrong Password!";
                } else {
                    $this -> error = "User does not exist!";
                }
                mysql_close($conn);
            }

        }

    }

    public function doLogout() {
        // delete the session of the user
        $_SESSION = array();
        session_destroy();
        header("Location:index.php");
    }

    public function isChemLoggedIn() {
        if (isset($_SESSION['patho_login_status']) AND $_SESSION['patho_login_status'] == 1) {
            return true;
        }
        return false;
    }

}
?>
