var daySelect,groupSelect;
var slotsDiv,timeInput;

document.body.onload = function(){
    daySelect = document.getElementById("daySelect");
    groupSelect = document.getElementById("groupSelect");
    slotsDiv = document.getElementById("slots");
    timeInput = document.getElementById("timeInput");
}

function generateData(){
    data = new FormData();
    if(!timeInput.value && timeInput.value <=0) {
        alert("duration for test is not valid");
        return false;
    }
    data.append('duration',timeInput.value);
    if(!groupSelect.value) {
        alert("group not selected");
        return false;
    }
    data.append('group_id',groupSelect.value);
    if(!daySelect.value) {
        alert("day not selected");
        return false;
    }
    data.append('day',daySelect.value);
    d = '';
    els = document.getElementsByClassName('slot');
    for(i=0;i<els.length;i++){
        d+=els[i].getElementsByClassName('startTime')[0].value;
        d+=els[i].getElementsByClassName('endTime')[0].value;;
    }
    data.append('times',d);
    return data;
}

function setData(data){
    timeInput.value = data.duration;
    if(data.times.length == 0){
        slotsDiv.innerHTML = "No slots on this day for this group" ;
        return;
    }
    ht = '';
    for(i=0;i<data.times.length/8;i++){
        ht +=   '<div class="slot">'+
                    'Start Time:<input type="text" class="startTime" placeholder="HHMM" value="'+data.times.substr(i*8,i*8+4)+'">'+
                    'End Time<input type="text" class="endTime" placeholder="HHMM" value="'+data.times.substr(i*8+4,i*8+8)+'">'+
                    '<button class="deleteButton">Delete</button>'+
                '</div>';
    }
    slotsDiv.innerHTML = ht;
}

function saveData(){
    data = generateData();
    if(!data){
        return;
    }
    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function(){
        if(ajax.readyState == 4 && ajax.status==200){
            alert(ajax.responseText);
        }
    }
    ajax.open('POST','saveGroupTimesDataAjax.php',true);
    ajax.send(data);
}

function onSelectChange(){
    if(daySelect.value && groupSelect.value){
        ajax=new XMLHttpRequest();
        ajax.onreadystatechange=function(){
            if(ajax.readyState==4 && ajax.status==200){
                try{
                    setData(JSON.parse(ajax.responseText));
                }catch(err){
                    slotsDiv.innerHTML = ajax.responseText;
                }
            }
        }
        link = "getGroupDetailsByDayAndId.php?day="+daySelect.value+"&group_id="+groupSelect.value;
        ajax.open("GET",link,true);
        ajax.send();
    }
}

function newSlot () {
    el = document.createElement('div');
    el.classList.add('slot');
    el.innerHTML = 'Start Time:<input type="text" class="startTime" placeholder="HHMM" >'+
                    'End Time :<input type="text" class="endTime" placeholder="HHMM">'+
                    '<button class="deleteButton">Delete</button>';
    if(slotsDiv.getElementsByClassName('slot').length == 0) {
        slotsDiv.innerHTML = "";
    }
    slotsDiv.appendChild(el);
}

document.onclick = function(event){
    if(event.target.className=='deleteButton'){
        event.target.parentNode.remove();
    }
}
