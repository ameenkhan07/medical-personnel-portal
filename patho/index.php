<?php
    session_start() or die("Internal Error");
    if (!(isset($_SESSION['patho_login_status']) AND ($_SESSION['patho_login_status'] == 1))){
        header("Location:login/index.php");
        exit();
    }
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Pathologist</title>
        <link href="style.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../patho/extrafiles/patho_list.js"></script>
        <script type="text/javascript" src="../patho/extrafiles/tests.js"></script>
        <script type="text/javascript" src="../patho/extrafiles/testsids.js"></script>
        <script type="text/javascript" src="../patho/extrafiles/testgroupsids.js"></script>
        <script type="text/javascript" src="../patho/extrafiles/testgroups.js"></script>
        <script type="text/javascript" src="../patho/extrafiles/groups.js"></script>
        <script type="text/javascript" src="../patho/suggestion.js"></script>
    </head>
    <body>
        <table id="header">
            <tr>
                <td class="navL"><a href="index.php"><img src="patho.jpg" width="33px" height="33px"></a></td>
                <td class="navM"><a href="index.php">Orders</a></td>
                <td class="navM"><a href="index.php?service=1">Tests</a></td>
                <td class="navM"><a href="index.php?times=1">Times</a></td>
                <td class="navR"><a href="login/index.php?logout"><img src="../images/settings.png"/></a></td>
            </tr>
        </table>

        <?php
        if (isset($_GET["service"])) {
            require_once ("service.php");
        } elseif(isset($_GET['setLocation'])){
            require_once ("setLocation.php");
        }elseif(isset($_GET['times'])){
            require_once ("times.php");
        }else{
            require_once ("orders.php");
        }
        ?>
    </body>
</html>
