<style>
#address {
    font-size: x-large;
    height: 40px;
    width: 70%;
}

#mapDiv {
    width:100%;
}
</style>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp" type="text/javascript"></script>
<script>
    var searchInput,geocoder,map,marker;

    function initialize() {
        mapDiv = document.getElementById('mapDiv');
        searchInput = document.getElementById('address');
        mapDiv.style.height = (window.innerHeight - 85) + 'px';
        var mapOptions = {center: { lat: 28.535364427922303, lng: 77.21867267941684},zoom: 11};
        map = new google.maps.Map(document.getElementById('mapDiv'),mapOptions);
        geocoder = new google.maps.Geocoder();
        google.maps.event.addListener(map, 'click', function(event) {
            if(marker) marker.setMap(null);
            marker = null;
            marker = new google.maps.Marker({map: map,position: event.latLng});
        });

        searchInput.onkeyup = function(event){
            if(event.keyCode == 13){
                search();
            }
        }

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        }
    }

    function showPosition(position){
        if(marker) marker.setMap(null);
        marker = null;
        marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(position.coords.latitude,position.coords.longitude)});
    }

    function geocodeCallBack(results,status){
        if(status == google.maps.GeocoderStatus.OK){
            map.setCenter(results[0].geometry.location);
            map.fitBounds(results[0].geometry.viewport);
            if(marker) marker.setMap(null);
            marker = null;
            marker = new google.maps.Marker({map: map,position: results[0].geometry.location});

        }else{
            alert('Geocode was not successful for the following reason: ' + status);
        }
    }

    function search(){
        var address = searchInput.value;
        geocoder.geocode({'address':address},geocodeCallBack);
    }

    function save(){
        if(!marker) alert('Position not selected');
        var form = new FormData();
        form.append('lat',marker.position.lat());
        form.append('lng',marker.position.lng());
        ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function(){
            if(ajax.readyState == 4 && ajax.status == 200){
                alert(ajax.responseText);
            }
        }
        ajax.open('POST','setLocationAjax.php',true);
        ajax.send(form);
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<div align="center">
    <button onclick="save()">Save</button>
    <input type="address" id="address" placeHolder="Type your address">
    <button onclick="search()">Search</button>
    <div id="mapDiv"></div>
</div>
