var order,orders,orderContainer;
var files;
var imageTypes = ['png','jpg','jpeg'];

var dht = '<input type="hidden" value="0" name="oid" id="oidInput"><br>'+
    '<input type="text" name="name[]"><input type="file" name="upfile[]" placeholder="File Name">';

var statusValues = ['0','1'];

document.body.onload = function(){
    orders = JSON.parse(document.orders);
    testsuginit();
    orderContainer = document.getElementById('orderContainer');
}

function loadOrder(){
    od = orders[parseInt(order.id)];
    orderContainer.innerHTML = '';
    document.forms.namedItem("uploadReportForm").innerHTML = dht;
    document.getElementById('oidInput').value=order.id;
    document.getElementById('ordersStatus').value=order.status;
    for(gid in od){
        el = document.createElement('div');
        el.className = 'group';
        h = 'Test Group:'+document.groupsIdsList[gid]+'<br>';
        h += 'Start Time:'+new Date(parseInt(od[gid]['s'])).toString().substr(0,24)+'<br>';
        h += 'End Time:'+new Date(parseInt(od[gid]['e'])).toString().substr(0,24)+'<br>';
        h += 'Tests:' + od[gid]['tests'];
        el.innerHTML = h;
        orderContainer.appendChild(el);
    }
    loadFiles();
}

document.onclick = function(event){
    if(event.target.className=='order'){
        if(order)
            order.classList.remove('selected');
        event.target.classList.add('selected');
        order=event.target;
        loadOrder();
    }
}

function addAnotherFile(){
    els = document.getElementsByName('upfile[]');
    for(i=0;i<els.length;i++){
        if(!els[i].value){
            alert("Some files are not selected");
            return;
        }
    }

    els = document.getElementsByName('name[]');
    for(i=0;i<els.length;i++){
        if(!els[i].value){
            alert("Some file name are empty");
            return;
        }
    }

    el = document.createElement('INPUT');
    el.setAttribute("type","text");
    el.setAttribute("name","name[]");
    el.setAttribute("placeHolder","File Name");
    document.getElementById('uploadReportForm').appendChild(el);

    el = document.createElement('INPUT');
    el.setAttribute("type","file");
    el.setAttribute("name","upfile[]");
    el.setAttribute("form","uploadReportForm");
    document.getElementById('uploadReportForm').appendChild(el);

}

function uploadFile(){
    if(document.getElementById('oidInput').value=='0'){
        alert("No order selected");
        return;
    }

    els = document.getElementsByName('upfile[]');
    for(i=0;i<els.length;i++){
        if(!els[i].value){
            alert("Some files are not selected");
            return;
        }
    }

    els = document.getElementsByName('name[]');
    for(i=0;i<els.length;i++){
        if(!els[i].value){
            alert("Some file name are empty");
            return;
        }
    }

    fileUploadProgressBar = document.getElementById('fileUploadBar');
    ajax = new XMLHttpRequest();
    data = new FormData(document.forms.namedItem("uploadReportForm"));
    ajax.onload = function(){
        if(ajax.readyState==4 && ajax.status==200){
            alert(ajax.responseText);
            document.forms.namedItem("uploadReportForm").innerHTML = dht;
            loadOrder();
        }
    }
    ajax.upload.onprogress = function (event){
        if (event.total != 0){
            fileUploadProgressBar.value = (event.loaded * 100) / event.total;
        }else{
            fileUploadProgressBar.removeAttribute('value');
        }
    }
    ajax.open('POST',"fileUpload.php",true);
    ajax.send(data);
}

function loadFiles(){
    ajax1 = new XMLHttpRequest();
    ajax1.onload = function(){
        try{
            files = JSON.parse(ajax1.responseText);
        }catch(err){
            alert(ajax1.responseText);
        }
        showFiles();
    }
    ajax1.open('GET','getFilesByOid?oid='+order.id,true);
    ajax1.send();
}

function showFiles(){
    ht = '';
    for(i=0;i<files.length;i++){
        f = files[i];
        fn = f.split('/');
        fn = fn[fn.length-1];
        ext = f.split('.');
        ext = ext[ext.length-1];
        if(imageTypes.indexOf(ext) == -1){
            ht += fn +' <a href="'+f+'">Download</a><br>';
        }else{
            ht += fn+'<br><img src="'+f+'"><br>';
        }
    }
    document.getElementById('files').innerHTML = ht;
}

function changeStatus(el){
    ajax = new XMLHttpRequest();
    ajax.onload = function(){
        if(ajax.readyState==4 && ajax.status==200){
            el.value=ajax.responseText;
        }
    }
    ajax.open('GET','changeOrderStatus.php?oid='+order.id+'&status='+el.value,true);
    ajax.send();
}
