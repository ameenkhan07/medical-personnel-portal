var testTable,tests=[];
var tableHead = '<tr>'+
                    '<td class="nameCell">Test Name</td>'+
                    '<td class="detailsCell">Details</td>'+
                    '<td class="actionCell">Action</td>'+
                '</tr>';
var edit;

document.body.onload = function (){
    testsuginit();
    testTable = document.getElementById("tests");
    testTable.innerHTML = tableHead;
    search_input = document.getElementById('search_input');
    search_input.onkeyup = function (event){
        if(event.keyCode == 13){
            id = document.testList[search_input.value];
            if(id) getTestById(id);
            else search();
        }
    }
}


function getTestById(id){
    if(tests.indexOf(id) != -1){
        return;
    }
    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function(){
        if(ajax.readyState==4 && ajax.status==200){
           var test = new Object();
           test['id'] = id;
           test['details'] = ajax.responseText;
           addTestToTable(test);
        }
    }
    ajax.open('GET','getTestById.php?id='+id,true);
    ajax.send();
}

function save(){
    data = new FormData();
    data.append('data',generateData());
    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function(){
        if(ajax.readyState==4 && ajax.status==200){
            alert(ajax.responseText);
            tests = [];
            testTable.innerHTML = tableHead;
        }
    }
    ajax.open("POST","saveAjax.php",true);
    ajax.send(data);
}

function search(){
    ids = [];
    for (var test in document.testList){
        if(test.indexOf(search_input.value) != -1){
            ids[ids.length] = document.testList[test];
        }
    }
    getTestsByIds(ids);
}

function getTestsByIds(ids){
    data = new FormData();
    data.append('ids',JSON.stringify(ids));
    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function(){
        if(ajax.readyState==4 && ajax.status==200){
            try{
                datas = JSON.parse(ajax.responseText);
                for(test in datas) addTestToTable(datas[test]);
            }catch(err){
                alert(ajax.responseText);
            }
        }
    }
    ajax.open('POST','getTestsByIds.php',true);
    ajax.send(data);
}

function addTestToTable(test){
    tests[tests.length] = test.id;
    el = document.createElement('tr');
    el.id = test.id;
    el.innerHTML =  '<td class="nameCell">'+document.testidsList[test.id]+'</td>'+
                    '<td class="detailsCell">'+test.details+'</td>'+
                    '<td class="actionCell"><img src="delete.png" class="action"></td>';
    testTable.appendChild(el);
}

function deleteTest(id){
    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function(){
        if(ajax.readyState==4 && ajax.status==200){
            alert(ajax.responseText);
        }
    }
    ajax.open("GET","deleteTestAjax.php?id="+id,true);
    ajax.send();
}

function generateData(){
    var data = new Object();
    for (i=0;i<tests.length;i++){
        d=document.getElementById(tests[i]).getElementsByClassName('detailsCell')[0].innerHTML;
        data[tests[i]] = d;
    }
    return JSON.stringify(data);
}

document.onclick = function (event){
    if(event.target.className == "action"){
        id = event.target.parentNode.parentNode.id;
        event.target.parentNode.parentNode.remove();
        deleteTest(id);
    }

    if(event.target != edit){
        if(edit){
            edit.contentEditable = false;
            edit.classList.remove('edit');
        }
    }
    if(event.target.className == 'detailsCell'){
        edit = event.target;
        edit.contentEditable = true;
        edit.classList.add('edit');
    }
}

