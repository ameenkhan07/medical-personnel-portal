var map,markers=[],pathoList,geocoder,searchInput;
var testSelectList,testSearchList,testInput;
var patho=0;
var slotsDiv,date=new Date(),pickSlotDiv;
var testNames = [];
var days = ['sun','mon','tue','wed','thu','fri','sat'];
var testGroups;
var selectedOption,options;
var exHT =  "<img src=\"exit.jpg\" width=\"40\" height=\"40\" id=\"exitImage\" onclick=\"document.getElementById('papa').style.display='none';\">";


function init(){
    pathoList = JSON.parse(document.rawPathos);
    searchInput = document.getElementById("search");
    testSearchList = document.getElementById("testSearchList");
    testInput = document.getElementById("testInput");
    slotsDiv = document.getElementById("slotsDiv");
    pickSlotDiv = document.getElementById("pickSlotDiv");
    selectedOption = document.getElementById("testSearchOption");
    options = [selectedOption,document.getElementById("testSelectOption")];
    mapInit();
    testsuginit();

    testInput.onkeyup = function (event){
        if(event.keyCode == 13){
            if(testNames.indexOf(testInput.value) != -1){
                return;
            }else if(! document.testList[testInput.value]){
                return;
            }
            el = document.createElement('li');
            el.className = "selected test";
            el.onclick = onTestClick;
            el.innerHTML = '<span class="name">'+testInput.value+'</span>';
            testSearchList.appendChild(el);
            testNames[testNames.length] = testInput.value;
        }
    }
}

function onTestClick(){
    if(this.className == "test"){
        this.classList.add("selected");
    }else if(this.classList.contains("selected")) {
        this.classList.remove("selected");
    }
}

function onTestClicked(el){
    if(el.className == "test"){
        el.classList.add("selected");
    }else if(el.classList.contains("selected")) {
        el.classList.remove("selected");
    }
}

function mapInit(){
    mapDiv = document.getElementById('mapDiv');
    geocoder = new google.maps.Geocoder();
    if(document.place){
        searchInput.value = document.place;
        geocoder.geocode({'address':document.place},geocodeCallBackC);
        return;
    }
    createMap(null);
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    }
}

function showPosition(position){
    map.setCenter(new google.maps.LatLng(position.coords.latitude,position.coords.longitude));
}

function geocodeCallBackC(results,status){
    if(status == google.maps.GeocoderStatus.OK){
         createMap(results[0].geometry.location);
         map.fitBounds(results[0].geometry.viewport);
    }else{
        createMap(null);
    }
}

function createMap(c){
    if(!c) c = { lat: 28.535364427922303, lng: 77.21867267941684};
    var mapOptions = {
        center: c,
        zoom: 11,
        mapTypeControl: false,
        zoomControl:true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL
        }
    };

    map = new google.maps.Map(mapDiv,mapOptions);
    for(var i=0;i<pathoList.length/3;i++){
        var latlng = new google.maps.LatLng(parseFloat(pathoList[3*i+2]), parseFloat(pathoList[3*i+1]));
        markers[markers.length] = new google.maps.Marker({map: map,position: latlng});
        markers[markers.length-1].mydata = pathoList[3*i];
        markers[markers.length-1].shown = 1;
        google.maps.event.addListener(markers[markers.length-1], 'click', markerClicked);
    }
}


function markerClicked(){
    patho = this.mydata;
    showPathoDetails();
    showLog();
    document.getElementById('papa').style.display='block';
}

function geocodeCallBack(results,status){
    if(status == google.maps.GeocoderStatus.OK){
         map.setCenter(results[0].geometry.location);
         map.fitBounds(results[0].geometry.viewport);
    }else{
        alert('Geocode was not successful for the following reason: ' + status);
    }
}

document.onkeyup = function (event){
    if((event.keyCode == 13)&&(event.target==searchInput)){
        var address = searchInput.value;
        if(!geocoder) return;
        geocoder.geocode({'address':address},geocodeCallBack);
    }
}

function showPathoDetails(){
    pathoDetailsDiv =  document.getElementById('pathoDetailsDiv');
    pathoDetailsDiv.innerHTML = exHT+'<img src="loading.gif" class="loadingImage" width="64" height="64">';
    ajax1 = new XMLHttpRequest();
    ajax1.onreadystatechange = function(){
        if(ajax1.readyState==4 && ajax1.status==200){
            data = JSON.parse(ajax1.responseText);
            document.getElementById('pathoNameDiv').innerHTML = data['name'];
            ht = exHT;
            ht += 'Email  '+data['email']+'<br>';
            ht += 'Mobile '+data['tel'] + '<br>';
            ht += 'Address '+data['address'] + '<br>';
            pathoDetailsDiv.innerHTML = ht;
        }
    }
    ajax1.open("GET",'getPathoById.php?id='+patho,true);
    ajax1.send();
}

function getTests(){
    var tests = selectedOption.getElementsByClassName("name");
    res = [];
    for(i=0;i<tests.length;i++){
        if(! tests[i].parentNode.classList.contains("selected")) continue;
        t=tests[i].innerHTML;
        if(!document.testList[t]) continue;
        res[res.length] = [document.testList[tests[i].innerHTML],t];
    }
    return res;
}

function getTestGroups(tests){
    res = new Object();
    tmp = [];
    for (i=0;i<tests.length;i++){
      group = document.groups[tests[i][0]];
      if(tmp.indexOf(group) == -1){
          tmp[tmp.length] = group;
          res[group] = new Object();
          res[group]['tests'] = [tests[i]];
      }else{
          l=res[group]['tests'].length;
          res[group]['tests'][l] = tests[i];
      }
    }
    return res;
}

function showLog(){
    if(!patho){
        return;
    }
    groups = getTestGroups(getTests());
    showGroups(groups);
}

function placeOrder(){
    data = generateData();
    if(!data){
        alert("Some data is not valid");
    }
    form = new FormData();
    form.append('data',data);
    name = document.getElementById('cName').value;
    mobile = document.getElementById('cMobile').value;
    remark = document.getElementById('cRemark').value;
    if(name==''){
        alert('Name not given');
        return;
    }
    if(mobile==''){
        alert('Mobile not given');
        return;
    }
    form.append('mobile',mobile);
    form.append('name',name);
    form.append('pid',patho);
    form.append('remark',remark);
    form.append('aid',document.getElementById('appointSelect').value);

    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function (){
        if(ajax.readyState==4 && ajax.status ==200){
            document.body.innerHTML = ajax.responseText;
        }
    }
    ajax.open("POST","placeOrder.php",true);
    ajax.send(form);
}

function showGroups(groups){
    slotsDiv.innerHTML = "";
    testGroups = [];
    for(id in groups){
        el = document.createElement("DIV");
        el.id = id;
        testGroups[testGroups.length] = id;
        ht = "Test Group Name: "+document.groupsIdsList[id]+'<br>';
        for (var i=0;i<groups[id]['tests'].length;i++){
            ht += 'Test <span class="testName">'+groups[id]['tests'][i][1]+'</span> <br>';
        }
        ht +=
            '<div>'+
                '<button onclick="showFreeSlots('+el.id+')">Pick A slot</button>'+
                '<span class="slotValue">No Slot Selected</span>'
            '</div>';
        el.innerHTML = ht;
        slotsDiv.appendChild(el);
    }
}

function showFreeSlots(group_id){
    if(!patho){
        alert("No pathologist selected");
    }
    ajax = new XMLHttpRequest();
    ajax.open("GET","getNFreeSlots.php?pid="+patho+"&group_id="+group_id,false);
    ajax.send();
    nfslots = [];
    try{
        tmp = JSON.parse(ajax.responseText);
        for(i=0;i<tmp.length;i++){
            nfslots[i] = [];
            for(j=0;j<tmp[i].length;j++){
                nfslots[i][j] = parseInt(tmp[i][j]);
            }
        }
    }catch(err){
        alert(ajax.responseText);
    }
    day = days[date.getDay()];
    ajax = new XMLHttpRequest();
    ajax.open("GET","getFreeSlots.php?pid="+patho+"&group_id="+group_id+'&day='+day,false);
    ajax.send();
    duration = null;
    fslots = [];
    try{
        data = JSON.parse(ajax.responseText);
        duration = parseInt(data[1]) * 60000;
        data = data[0];
        for(i=0;i<data.length/8;i++){
            startTime = new Date(date.getTime());
            startTime.setHours(parseInt(data.substr(8*i,2)));
            startTime.setMinutes(parseInt(data.substr(8*i+2,2)));
            startTime.setSeconds(0);
            endTime = new Date(date.getTime());
            endTime.setHours(parseInt(data.substr(8*i+4,2)));
            endTime.setMinutes(parseInt(data.substr(8*i+6,2)));
            endTime.setSeconds(0);

            if(endTime.getTime() < new Date().getTime()){
                continue;
            }

            fslots[fslots.length] = [startTime.getTime(),endTime.getTime()];
        }
    }catch(err){
        alert(ajax.responseText);
    }
    slots = [];
    for (i=0;i<fslots.length;i++){
        n = (fslots[i][1] - fslots[i][0]) / duration;
        for(j=0;j<n;j++){
            slot = [fslots[i][0]+duration*j,fslots[i][0]+duration*(j+1)];
            if (!isInSlots(slot,nfslots)){
                slots[slots.length] = slot;
            }
        }
    }
    putToSlotDiv(slots,group_id);
}

function putToSlotDiv(slots,group_id){
    ht =    '<img id="exitIcon" src="delete.png" width="16" height="16" onclick="pickSlotDiv.style.display=\'none\'">'
            +'<div id="wraper1"><button onclick="prevDay('+group_id+')">Prev</button>'+
            '<span id="slotDate">'+date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear()+'</span>'+
            '<button onclick="nextDay('+group_id+')">Next</button></div>';
    for (i=0;i<slots.length;i++){
        d=new Date(slots[i][0]);
        ht += '<div class="slotTime" s="'+slots[i][0]+'" e="'+slots[i][1]+'" g="'+group_id+'" >'+makeTwoDigit(d.getHours())+':'+makeTwoDigit(d.getMinutes())+'</div>'
    }
    if(slots.length==0){
        ht += 'No slots on this day';
    }
    pickSlotDiv.innerHTML = ht;
    pickSlotDiv.style.display = "block";
}

function nextDay(group_id){
    date = new Date(date.getTime() + 86400000);
    showFreeSlots(group_id);
}

function prevDay(group_id){
    date = new Date(date.getTime() - 86400000);
    showFreeSlots(group_id);
}

function isInSlots(slot,slots){
    for(var i=0;i<slots.length;i++){
        if (((slot[0]>=slots[i][0])&&(slot[1]<=slots[i][1]))||((slot[1]>=slots[i][0])&&(slot[1]<=slots[i][1])))
            return true;
    }
    return false;
}

function makeTwoDigit(d){
    if(d<10)
        return '0'+d;
    else
        return d;
}

document.onclick = function (event){
    if(event.target.className=='slotTime'){
        s = new Date(parseInt(event.target.getAttribute('s')));
        e = new Date(parseInt(event.target.getAttribute('e')));
        g = event.target.getAttribute('g');
        t = "On "+s.getDate()+'/'+(s.getMonth()+1)+'/'+s.getFullYear()+ " FROM "+s.getHours()+":"+makeTwoDigit(s.getMinutes()) + " To "+e.getHours()+":"+ makeTwoDigit(e.getMinutes());
        el = document.getElementById(g).getElementsByClassName("slotValue")[0];
        el.innerHTML = t;
        el.setAttribute('s',event.target.getAttribute('s'));
        el.setAttribute('e',event.target.getAttribute('e'));
    }
}

function generateData(){
    var data = new Object();
    for (i=0;i<testGroups.length;i++){
        if(parseInt(testGroups[i])==1){
            return;
        }
        t = new Object();
        el = document.getElementById(testGroups[i]);

        //add tests
        els = el.getElementsByClassName("testName");
        ts = [];
        for(j=0;j<els.length;j++){
            ts[ts.length] = els[j].innerHTML;
        }
        t['tests'] = ts;

        //add s and e
        elt = el.getElementsByClassName('slotValue')[0];
        t['s'] = elt.getAttribute("s");
        t['e'] = elt.getAttribute("e");
        if((! t['s']) || (! t['e'])){
            return false;
        }
        data[testGroups[i]] = t;
    }
    return JSON.stringify(data);
}

function select(el){
    selectedOption.style.display='none';
    selectedOption = options[el];
    selectedOption.style.display = 'block';
    if(selectedOption.id=='testSearchOption'){
        document.getElementById('testSelectList').innerHTML = '';
        document.getElementById('appointSelect').value = 0;
    }else{
        document.getElementById('testSearchList').innerHTML = '';
    }
}

function changeAppoint(el){
    a = new XMLHttpRequest();
    a.onreadystatechange = function (){
        if(a.readyState==4 && a.status==200){
            document.getElementById('testSelectList').innerHTML = a.responseText;
        }
    }
    a.open("GET","getAppointById.php?aid="+el.value,true);
    a.send();
}

function confirm(){
    el = document.getElementById('confirmDiv');
    el.className=='popup';
}

function proceed(){
    document.getElementById("popup").style.display = 'block';
}

