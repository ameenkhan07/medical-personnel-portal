<?php
    session_start();
    $dbhost  = '127.0.0.1';    // Unlikely to require changing
    $dbname  = 'orfom';       // Modify these...
    $dbuser_name  = 'orfom';   // ...variables according
    $dbpass  = 'awerty56';   // ...to your installation

    mysql_connect($dbhost, $dbuser_name, $dbpass) or die(mysql_error());
    mysql_select_db($dbname) or die(mysql_error());

    include 'function.php';

      if (isset($_SESSION['doc_name']))
      {
          $doc_name  = $_SESSION['doc_name'];
          $loggedin = TRUE;
          $doc_namestr  = " ($doc_name)";
           }

    else $loggedin = FALSE;
    if (!$loggedin) die();
    $subquery = "SELECT * FROM doctors_info WHERE doc_name='$doc_name'";
    $subresult = mysql_query($subquery);
    if (!$subresult) die ("Database access failed: " . mysql_error()); 
    $subrow = mysql_fetch_row($subresult);

    $subquery1 = "SELECT * FROM relation WHERE DID='$subrow[0]'";
    $subresult1 = mysql_query($subquery1);
    if (!$subresult1) die ("Database access failed: " . mysql_error()); 
    $subrow1number = mysql_num_rows($subresult1);

?>    
    <!-- HEADING -->
    <div class="container col-lg-12 text-center">
        <h3>Clinic Details</h3>       
    </div>
    
    <!-- FORM -->
    <form  class="form-horizontal col-lg-12" role="form" >

        <div class="form-group">
            <div class="col-lg-6 col-lg-offset-3">
                <input type="text" class="form-control text-center" id='clinicname' name='clinic' placeholder='Clinic Name'  required/>
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-6 col-lg-offset-3">
                <input type="text" class="form-control text-center" id='address' name='address' placeholder='Clinic Address' required/>            
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-6 col-lg-offset-3">
                <input type="text" class="form-control text-center" id='tel' name='tel' placeholder='Clinic Phone Number' required/>                        
            </div>
        </div>

        <div class="form-group text-center">
            <button class="btn btn-primary" name='register' value='Submit' onclick='clinicdetail();showmodal();'>
                Submit
            </button>
        </div>

    </form>
