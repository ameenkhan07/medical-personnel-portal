<html>
	<head>
		<link rel="stylesheet" type="text/css" href="register.css">
		<title>Chemist Registeration</title>
	</head>
	<body>
		<div class="error">
			<?php
			if (isset($registration)) {
				if ($registration -> error) {
					echo($registration -> error);
				}
			}
		?>
		</div>
		<div id="chem">ADD CHEMIST</div>
		<!-- register form -->
		<form method="post" action="index.php" name="registerform" id="form1">

			<div class="inputLabel">
				Name
			</div>
			<div class="inputErrorText"></div>
			<div class="inputErrorSymbol"></div>
			<input id="login_input_username" class="login_input" type="text" name="name" required >

			<div class="inputLabel">
				Email
			</div>
			<div class="inputErrorText"></div>
			<div class="inputErrorSymbol"></div>
			<input class="login_input" type="email"  name="email" required >

			<div class="inputLabel">
				Username
			</div>
			<div class="inputErrorText"></div>
			<div class="inputErrorSymbol"></div>
			<input class="login_input" type="text"  name="username" pattern="[a-zA-Z0-9]{2,64}" required >

			<div class="inputLabel">
				Age
			</div>
			<div class="inputErrorText"></div>
			<div class="inputErrorSymbol"></div>
			<input class="login_input" type="number"  name="age" required >

			<div class="inputLabel">
				Address
			</div>
			<div class="inputErrorText"></div>
			<div class="inputErrorSymbol"></div>
			<input class="login_input" type="text"  name="address" required >

			<div class="inputLabel">
				Qualification
			</div>
			<div class="inputErrorText"></div>
			<div class="inputErrorSymbol"></div>
			<input class="login_input" type="text"  name="qual" required >

			<div class="inputLabel">
				Mobile
			</div>
			<div class="inputErrorText"></div>
			<div class="inputErrorSymbol"></div>
			<input id="login_input_usernumber" class="login_input" type="number"  name="tel" required >

			<div class="inputLabel">
				Password (min. 6 characters)
			</div>
			<div class="inputErrorText"></div>
			<div class="inputErrorSymbol"></div>
			<input id="login_input_password_new" class="login_input" type="password" name="password" pattern=".{6,}" required autocomplete="off" >

			<div class="inputLabel">
				Repeat password
			</div>
			<div class="inputErrorText"></div>
			<div class="inputErrorSymbol"></div>
			<input id="login_input_password_repeat" class="login_input" type="password" name="password_repeat" pattern=".{6,}" required autocomplete="off" >

		</form>
		<div class="enterimage">
			<input type="submit"  form="form1" name="register" value="">
		</div>
		<!-- backlink -->
		<div class="back">
			<a href="adminmain.php" style="color:#fff; text-decoration:none"> Back <img src="../../img/back.png" style="margin:-2px">
		</div>
	</body>
</html>
