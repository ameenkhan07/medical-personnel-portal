<?php


class DocRegistration
{
   
    private $db_connection = null;
  
    public $errors = array();
  
    public $messages = array();

   
    public function __construct()
    {
        if (isset($_POST["register"])) {
            $this->registerNewUser();
        }
    }

 
    private function registerNewUser()
    {
       /* if (empty($_POST['doc_name'])) {
            $this->errors[] = "Empty Username";
		}
			
		elseif (empty($_POST['doc_name'])) {
            $this->errors[] = "Empty Username";
        } 
		elseif (empty($_POST['user_address'])) {
            $this->errors[] = "Empty Useraddress";
        } 
		elseif (empty($_POST['user_number'])) {
            $this->errors[] = "Empty Usernumber";
        } 
		elseif (empty($_POST['user_bg'])) {
            $this->errors[] = "Empty User blood group";
        } 
		elseif (empty($_POST['user_age'])) {
            $this->errors[] = "Empty User age";
        } 
			
			
			
			
			
        elseif (empty($_POST['user_password_new']) || empty($_POST['user_password_repeat'])) {
            $this->errors[] = "Empty Password";
        } elseif ($_POST['user_password_new'] !== $_POST['user_password_repeat']) {
            $this->errors[] = "Password and password repeat are not the same";
        } elseif (strlen($_POST['user_password_new']) < 6) {
            $this->errors[] = "Password has a minimum length of 6 characters";
        } elseif (strlen($_POST['doc_name']) > 64 || strlen($_POST['doc_name']) < 2) {
            $this->errors[] = "Username cannot be shorter than 2 or longer than 64 characters";
        } elseif (!preg_match('/^[a-z\d]{2,64}$/i', $_POST['doc_name'])) {
            $this->errors[] = "Username does not fit the name scheme: only a-Z and numbers are allowed, 2 to 64 characters";
        } elseif (empty($_POST['user_email'])) {
            $this->errors[] = "Email cannot be empty";
        } elseif (strlen($_POST['user_email']) > 64) {
            $this->errors[] = "Email cannot be longer than 64 characters";
        } elseif (!filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)) {
            $this->errors[] = "Your email address is not in a valid email format";
        } elseif (!empty($_POST['doc_name'])
            && strlen($_POST['doc_name']) <= 64
            && strlen($_POST['doc_name']) >= 2
            && preg_match('/^[a-z\d]{2,64}$/i', $_POST['doc_name'])
            && !empty($_POST['user_email'])
			
			&& !empty($_POST['user_address'])
			&& !empty($_POST['user_number'])
			&& !empty($_POST['user_bg'])
			&& !empty($_POST['user_age'])
			
			
            && strlen($_POST['user_email']) <= 64
            && filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)
            && !empty($_POST['user_password_new'])
            && !empty($_POST['user_password_repeat'])
            && ($_POST['user_password_new'] === $_POST['user_password_repeat'])
        ) {
		
		*/
            // create a database connection
            $this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

            // change character set to utf8 and check it
            if (!$this->db_connection->set_charset("utf8")) {
                $this->errors[] = $this->db_connection->error;
            }

            // if no connection errors (= working database connection)
            if (!$this->db_connection->connect_errno) {

                // escaping, additionally removing everything that could be (html/javascript-) code
				//////////////////////////////////////////////////////////////////////////////////////////////////
                $name = $this->db_connection->real_escape_string(strip_tags($_POST['name'], ENT_QUOTES));
                $dep = $this->db_connection->real_escape_string(strip_tags($_POST['dep'], ENT_QUOTES));
				$address = $this->db_connection->real_escape_string(strip_tags($_POST['address'], ENT_QUOTES));
				$qual = $this->db_connection->real_escape_string(strip_tags($_POST['qual'], ENT_QUOTES));
				$gen = $this->db_connection->real_escape_string(strip_tags($_POST['gen'], ENT_QUOTES));
				$doc_name = $this->db_connection->real_escape_string(strip_tags($_POST['doc_name'], ENT_QUOTES));
				$yoe = $this->db_connection->real_escape_string(strip_tags($_POST['yoe'], ENT_QUOTES));
				$tel = $this->db_connection->real_escape_string(strip_tags($_POST['tel'], ENT_QUOTES));
				$fee = $this->db_connection->real_escape_string(strip_tags($_POST['fee'], ENT_QUOTES));
				$bloodg = $this->db_connection->real_escape_string(strip_tags($_POST['bloodg'], ENT_QUOTES));
				
				

                $doc_password = $_POST['doc_password_new'];

               
                $doc_password_hash = password_hash($doc_password, PASSWORD_DEFAULT);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // check if user or email address already exists
                $sql = "SELECT * FROM doctors_info WHERE doc_name = '" . $doc_name . "'";
                $query_check_doc_name = $this->db_connection->query($sql);

                if ($query_check_doc_name->num_rows == 1) {
                    $this->errors[] = "Sorry, that username / email address is already taken.";
                } else {
                    // write new user's data into database
////////////////////////					/////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    $sql = "INSERT INTO doctors_info (name, doc_password_hash, dep, address, qual, gender, doc_name, yoe, tel,fee, bloodg)
                           VALUES('".$name . "', 
						   '" . $doc_password_hash . "',
						   '" . $dep . "',
						   '" . $address . "',
						   '" . $qual . "',
						   '" . $gen . "',
						   '" . $doc_name . "',
						   
						   '" . $yoe . "', 
						   '" . $tel . "',
						  '". $fee ." ',
						   '" . $bloodg . "');";
					$query_new_user_insert = $this->db_connection->query($sql);

                    // if user has been added successfully
                    if ($query_new_user_insert) {
                        $this->messages[] = "New Doctor  has been added successfully.";
						?>
						<form action="work.php" method="get">
						<input type="hidden" name="did" value="<?php echo $doc_name ?>" >
						<input type="submit" name="go" value="OK" >
						</form>
						<?php
                    } else {
                        $this->errors[] = "Sorry, your registration failed. Please go back and try again.";
                    }
                }
            } else {
                $this->errors[] = "Sorry, no database connection.";
            }
        
		//else {
          //  $this->errors[] = "An unknown error occurred.";
        //}
    }
	}

