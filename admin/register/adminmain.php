<?php session_start();
date_default_timezone_set('Asia/Calcutta');?>
<html>
 <head>
   <link rel="stylesheet" type="text/css" href="style2.css">
   <link rel="stylesheet" type="text/css" href="button.css">
   <style>
table, td, th {
    border: 1px solid green;
}

th {
    background-color: chocolate;
    color: white;
}
</style>
<script type="text/javascript">
          function doc(val)
     {var  xhr=new XMLHttpRequest();
	   xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
      document.getElementById("doc").innerHTML=xhr.responseText;
     }
    }
	  xhr.open("GET","doctor.php?q="+val,true);
      xhr.send();
	}
	   function recname(val)
     {var  xhr=new XMLHttpRequest();
	   xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
      document.getElementById("content").innerHTML=xhr.responseText;
     }
    }
	  xhr.open("GET","recname.php?q="+val,true);
      xhr.send();
	}
	
	function showSelected() 
	     { var  xhr=new XMLHttpRequest();

    var sel   = document.getElementById('depts') || false,
        url   = '',
        query = [];
    
    if (!sel) {
        return false;
    }
    
    for (var i = 0, m = sel.length; i < m; i++) {
        if (sel[i].selected) {
            query.push(sel[i].value);
        }
    }
    
    url = query.join('&sl[]=');
	xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
      document.getElementById("doc").innerHTML=xhr.responseText;
     }
    }
    
     xhr.open("GET","addmul.php?sl[]="+url,true);
      xhr.send();
    
}

function showdocSelected() 
	     { var  xhr=new XMLHttpRequest();
           var name=document.getElementById('docname').value;
		   var id=document.getElementById('doctorid').value;
	xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
      document.getElementById("content").innerHTML=xhr.responseText;
     }
    }
    
     xhr.open("GET","addbysearch.php?name="+name+"&did="+id,true);
      xhr.send();
    
}
function addgivendoc(val) 
	     { var  xhr=new XMLHttpRequest();
         
	xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
      document.getElementById("content").innerHTML=xhr.responseText;
     }
    }
    
     xhr.open("GET","adddocbysearch.php?did="+val,true);
      xhr.send();
    
}
	 function adddep()
{	    var  xhr=new XMLHttpRequest();
	   xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
       document.getElementById("content").innerHTML=xhr.responseText;
	   document.getElementById('doc').innerHTML="";
     }
    }
	  xhr.open("GET","adddepart.php",true);
      xhr.send();
	 
	 }
	 
	 function addrec()
{	    var  xhr=new XMLHttpRequest();
	   xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
       document.getElementById("content").innerHTML=xhr.responseText;
	   document.getElementById("doc").innerHTML="";
     }
    }
	  xhr.open("GET","newrecept.php",true);
      xhr.send();
	 
	 }
	  function addchem()
{	    var  xhr=new XMLHttpRequest();
	   xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
       document.getElementById("content").innerHTML=xhr.responseText;
	   document.getElementById("doc").innerHTML="";
     }
    }
	  xhr.open("GET","newchem.php",true);
      xhr.send();
	 
	 }
	   function addpatho()
{	    var  xhr=new XMLHttpRequest();
	   xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
       document.getElementById("content").innerHTML=xhr.responseText;
	   document.getElementById("doc").innerHTML="";
     }
    }
	  xhr.open("GET","newpatho.php",true);
      xhr.send();
	 
	 }
	 function adddoc()
{	    var  xhr=new XMLHttpRequest();
	   xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
       document.getElementById("content").innerHTML=xhr.responseText;
     }
    }
	  xhr.open("GET","adddoc.php",true);
      xhr.send();
	 
	 }
	 
	 function addnewdoc()
{	    var  xhr=new XMLHttpRequest();
        var name=document.getElementById('name').value;
	    var address=document.getElementById('address').value;
        var dep=document.getElementById('newdep').value;
	    var qualification=document.getElementById('qualification').value;
        var age=document.getElementById('age').value;
        var gender=document.getElementById('gender').value;
        var blood=document.getElementById('bloodgrp').value;
        var username=document.getElementById('username').value;
		var fee=document.getElementById('fee').value;
		var yoe=document.getElementById('yoe').value;
        var passnew=document.getElementById('password_new').value;
        var passrepeat=document.getElementById('password_repeat').value;
        var tel=document.getElementById('telephone').value;

	   xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
       document.getElementById("content").innerHTML=xhr.responseText;
     }
    }
	  xhr.open("GET","addnewdoc.php?name="+name+"&address="+address+"&username="+username+"&dep="+dep+"&qual="+qualification+"&age="+age+"&gender="+gender+"&blood="+blood+"&tel="+tel+"&passnew="+passnew+"&passrepeat="+passrepeat+"&fee="+fee+"&yoe="+yoe,true);
      xhr.send();
	 
	 }
	  function addnewrecep()
{	    var  xhr=new XMLHttpRequest();
        var name=document.getElementById('name').value;
	    var address=document.getElementById('address').value;
        var email=document.getElementById('email').value;
	    var qualification=document.getElementById('qualification').value;
        var age=document.getElementById('age').value;
        var gender=document.getElementById('gender').value;
        var blood=document.getElementById('bloodgrp').value;
        var username=document.getElementById('username').value;
        var passnew=document.getElementById('password_new').value;
        var passrepeat=document.getElementById('password_repeat').value;
        var tel=document.getElementById('telephone').value;

	   xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
       document.getElementById("content").innerHTML=xhr.responseText;
     }
    }
	  xhr.open("GET","addnewrecep.php?name="+name+"&address="+address+"&username="+username+"&email="+email+"&qual="+qualification+"&age="+age+"&gender="+gender+"&blood="+blood+"&tel="+tel+"&passnew="+passnew+"&passrepeat="+passrepeat,true);
      xhr.send();
	 
	 }
	
	 	  function addnewpatho()
{	    var  xhr=new XMLHttpRequest();
        var name=document.getElementById('name').value;
	    var address=document.getElementById('address').value;
        var email=document.getElementById('email').value;
	    var qualification=document.getElementById('qual').value;
        var age=document.getElementById('age').value;
        var gender=document.getElementById('gender').value;
        var blood=document.getElementById('blood').value;
        var username=document.getElementById('username').value;
        var passnew=document.getElementById('password_new').value;
        var passrepeat=document.getElementById('password_repeat').value;
        var tel=document.getElementById('telephone').value;

	   xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
       document.getElementById("content").innerHTML=xhr.responseText;
     }
    }
	  xhr.open("GET","addnewpatho.php?name="+name+"&address="+address+"&username="+username+"&email="+email+"&qual="+qualification+"&age="+age+"&gender="+gender+"&blood="+blood+"&tel="+tel+"&passnew="+passnew+"&passrepeat="+passrepeat,true);
      xhr.send();
	 
	 }
	  function addnewchem()
{	    var  xhr=new XMLHttpRequest();
        var name=document.getElementById('name').value;
	    var address=document.getElementById('address').value;
        var email=document.getElementById('email').value;
	    var qualification=document.getElementById('qualification').value;
        var age=document.getElementById('age').value;
        var username=document.getElementById('username').value;
        var passnew=document.getElementById('password_new').value;
        var passrepeat=document.getElementById('password_repeat').value;
        var tel=document.getElementById('telephone').value;

	   xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
       document.getElementById("content").innerHTML=xhr.responseText;
     }
    }
	  xhr.open("GET","addnewchem.php?name="+name+"&address="+address+"&username="+username+"&email="+email+"&qual="+qualification+"&age="+age+"&tel="+tel+"&passnew="+passnew+"&passrepeat="+passrepeat,true);
      xhr.send();
	 
	 }
	function del(val)
     {var  xhr=new XMLHttpRequest();
	  var id=val;
	
	  var did=document.getElementById('doctorid').value;
	  var start=document.getElementById('start').value;
	  var end=document.getElementById('end').value;
	 
	   xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
      document.getElementById("content").innerHTML=xhr.responseText;
     }
    }
	  xhr.open("GET","del.php?did="+did+"&id="+id+"&start="+start+"&end="+end,true);
      xhr.send();
	}
	    function save(val)
     {var  xhr=new XMLHttpRequest();
	  var id=val;
	
	  var did=document.getElementById('doctorid').value;
	  var start=document.getElementById('start').value;
	  var end=document.getElementById('end').value;
	 
	   xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
      document.getElementById("content").innerHTML=xhr.responseText;
     }
    }
	  xhr.open("GET","save.php?did="+did+"&id="+id+"&start="+start+"&end="+end,true);
      xhr.send();
	}
	  function slot()
     {var  xhr=new XMLHttpRequest();
	  
	
	  var did=document.getElementById('dids').value;
	  var day=document.getElementById('d').value;
	  var start=document.getElementById('start1').value;
	  var end=document.getElementById('end1').value;
	   var slot=document.getElementById('slot1').value;
	   
	   xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
      document.getElementById("content").innerHTML=xhr.responseText;
     }
    }
	  xhr.open("GET","insertslot.php?did="+did+"&start="+start+"&end="+end+"&day="+day+"&slot="+slot,true);
      xhr.send();
	}
	
	 function docdetail(val)
	 { var  xhr=new XMLHttpRequest();
	   xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
      document.getElementById("content").innerHTML=xhr.responseText;
     }
    }
	  xhr.open("GET","docdetail.php?q="+val,true);
      xhr.send();
	 }
	 
	 function adddept()
	 {var  xhr=new XMLHttpRequest();
	 var dep=document.getElementById('department').value;
	   xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
      document.getElementById("content").innerHTML=xhr.responseText;
     }
    }
	  xhr.open("GET","addnewdept.php?dep="+dep,true);
      xhr.send();
	 
	 
	 
	 }
	 
	 
	 
	 
	 
	</script>
      <script type="text/javascript" >
     function doctor()
     {var  xhr=new XMLHttpRequest();
	   xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
      document.getElementById("submenu").innerHTML=xhr.responseText;
	  	        document.getElementById("content").innerHTML="";

     }
    }
	  xhr.open('GET','dep.php',true);
      xhr.send();
	}
	  function rec()
     {var  xhr=new XMLHttpRequest();
	   xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
      document.getElementById("submenu").innerHTML=xhr.responseText;
	  document.getElementById("doc").innerHTML="";
	        document.getElementById("content").innerHTML="";

     }
    }
	  xhr.open('GET','rec.php',true);
      xhr.send();
	}
	 function patho()
     {var  xhr=new XMLHttpRequest();
	   xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
      document.getElementById("submenu").innerHTML=xhr.responseText;
	   document.getElementById("doc").innerHTML="";
	        document.getElementById("content").innerHTML="";
     }
    }
	  xhr.open('GET','patho.php',true);
      xhr.send();
	}
	 function chem()
     {var  xhr=new XMLHttpRequest();
	   xhr.onreadystatechange=function() {
  if (xhr.readyState==4 && xhr.status==200) {
      document.getElementById("submenu").innerHTML=xhr.responseText;
	   document.getElementById("doc").innerHTML="";
	        document.getElementById("content").innerHTML="";
     }
    }
	  xhr.open('GET','chem.php',true);
      xhr.send();
	}
	</script>
	 <title>
	  Admin
	  </title>
 </head>
<body >
    <div class="header">
<?php

$dbhost  = 'localhost';    // Unlikely to require changing
$dbname  = 'orfom';       // Modify these...
$dbuser  = 'orfom';   // ...variables according
$dbpass  = 'awerty56';   // ...to your installation
// ...and preference

mysql_connect($dbhost, $dbuser, $dbpass) or die(mysql_error());
mysql_select_db($dbname) or die(mysql_error());
 // Example 21-1: functions.php
 
function createTable($name, $query)
{
    queryMysql("CREATE TABLE IF NOT EXISTS $name($query)");
    echo "Table '$name' created or already exists.<br />";
}

function queryMysql($query)
{
    $result = mysql_query($query) or die(mysql_error());
     return $result;
}

function destroySession()
{
    $_SESSION=array();
    
    if (session_id() != "" || isset($_COOKIE[session_name()]))
        setcookie(session_name(), '', time()-2592000, '/');

    session_destroy();
}

function sanitizeString($var)
{
    $var = strip_tags($var);
    $var = htmlentities($var);
    $var = stripslashes($var);
    return mysql_real_escape_string($var);
}


function showappointment($doc,$hid)
{   $did='';
   $result=queryMysql("Select did from doctors_info where doc_name='$doc' ");
     while($row=mysql_fetch_array($result))
	  { $did=$row['did'];
	  
	  }
	   
	  $query1="Select DISTINCT name,dep,qual,rating,yoe ,fee from doctors_info where did=$did ";
			 $result1=mysql_query($query1) or die(" Doctor Not Found");
			 $n= mysql_num_rows($result1);
			  
			 while($row=mysql_fetch_array($result1))
			 {  echo"<fieldset style='width:300px;color:blue;margin-top:150px;background:#fff;'>";
			 echo"<legend>Doctor's Details </legend>";
			    $name=$row['name'];
			     $dep=$row['dep'];
				 $qual=$row['qual'];
				 $rating=$row['rating'];
				 $yoe=$row['yoe'];
				 $fee=$row['fee'];
	            echo "Dr.".ucfirst($name);
				echo"</br>Department:&nbsp".$dep;
				echo"</br>Qualification:&nbsp".$qual;
				echo "</br>Rating:&nbsp".$rating;
				echo "</br>Experience of &nbsp".$yoe."&nbsp years";
				echo "</br>Consultation Fee: Rs.".$fee;
			 
			 echo "</fieldset>";
			 }
			 		

			 echo "<table style='margin-left:250px;margin-top:100px;background:#fff;'>";
			 			 			 echo "<caption style='color:#3b988e'><b>Dr.".ucfirst($doc) ."'s Appointments:&nbsp</b></caption>";

			 echo "<th>Today's Appointments</th><th>Last 7 days Appointments</th><th>Last Month Appointments</th><th>Total Appointments</th><tr>";
	  $result3=queryMysql("Select COUNT(AID) from appointment where did=$did and hid=$hid and date=date('Y-m-d') ");
        while($row3=mysql_fetch_row($result3))
		   { echo "<td>";
		   foreach($row3 as $val)
	    echo $val;
		echo"</td>";
	  } 
	      echo"<td>";
	  $date=date("Y-m-d");
$newdate=strtotime("-7 Day",strtotime($date));
$d=date("Y-m-d",$newdate);
$t=date("Y-m-d");
   $result2=queryMysql("Select COUNT(AID) from appointment where did=$did and hid=$hid and `date` between     DATE_SUB( CURDATE( ) ,INTERVAL 7 DAY ) and  CURDATE( ) ");
     while($row2=mysql_fetch_row($result2))
	  { foreach($row2 as $val)
	    echo $val;
		echo"</td>";
	  } 
	  $result3=queryMysql("Select COUNT(AID) from appointment where did=$did and hid=$hid and `date` between     DATE_SUB( CURDATE( ) ,INTERVAL 1 MONTH ) and  CURDATE( ) ");
     while($row3=mysql_fetch_row($result3))
	  {  echo "<td>";
	  foreach($row3 as $val)
	    echo $val;
		echo"</td>";
	  } 
	  
	  $res=queryMysql("Select COUNT(AID) from appointment where did=$did and hid=$hid ");
	  while($r=mysql_fetch_row($res))
	  { echo"<td>";
	  foreach($r as $val)
	    echo $val;
	  }
	  echo "</td>";
	  echo"</tr></table>";
	  
}


if (isset($_SESSION['user_name']))
{
    $user     = $_SESSION['user_name'];
	
    $loggedin = TRUE;
    $userstr  = " ($user)";
}

else $loggedin = FALSE;
if (!$loggedin) die();

$subquery = "SELECT * FROM admin WHERE user_name='$user'";
$subresult = mysql_query($subquery);
if (!$subresult) die ("Database access failed: " . mysql_error()); 
$subrow = mysql_fetch_row($subresult);


if ($loggedin)
{
echo <<<_end
    <div class="profilepic">
    <img src='praveen.jpg' style="  width:30px; height: 30px; border-radius:3px;"/>
    </div> <div class="profilename"> Mr. $user</div><div class="profileid"> $subrow[5]</div>
_end;
    $_SESSION['ADID']=$subrow[5];
	$_SESSION['HID']=$subrow[6];

}

?> 
        <div class="settings"><a href="../../logout.php" ><img src="settings2.png" style="border-radius:3px;margin-left: 200%;" ></a></div>  



     </div>
	  <div id='mainbody'style='background-color:#e9eaed'>
	  <div id='side' style='float:left;width:200px;'>
	     
         <input type='button' name='doctor' value='Doctors'style='width:150px;height:40px;color:#fff;background:#63b372;font-size:18px' onclick='doctor();'> 
         <input type='button' name='receptionist' value='Receptionist'style='width:150px;height:40px;color:#fff;background:#63b372;font-size:18px' onclick='rec();'>
         <input type='button' name='chemist' value='Chemist'style='width:150px;height:40px;color:#fff;background:#63b372;font-size:18px' onclick='chem();'>
         <input type='button' name='pathologist' value='Pathologist'style='width:150px;height:40px;color:#fff;background:#63b372;font-size:18px'onclick='patho();'>
		 </div>
                <div id='submenu' style='position:absolute;margin-left:150px'> </div>
		
				 <div id='doc' style='position:absolute;margin-left:300px;'></div>
                    <div id='content' style='position:absolute;margin-left:480px'>
		
		 		 </div>
		 </div>
</body>
</html>

