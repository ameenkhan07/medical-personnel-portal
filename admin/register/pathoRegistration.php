<?php


class PathoRegistration
{
   
    private $db_connection = null;
  
    public $errors = array();
  
    public $messages = array();

   
    public function __construct()
    {
        if (isset($_POST["register"])) {
            $this->registerNewUser();
        }
    }

 
    private function registerNewUser()
    {
      
            // create a database connection
            $this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

            // change character set to utf8 and check it
            if (!$this->db_connection->set_charset("utf8")) {
                $this->errors[] = $this->db_connection->error;
            }

            // if no connection errors (= working database connection)
            if (!$this->db_connection->connect_errno) {

                // escaping, additionally removing everything that could be (html/javascript-) code
				//////////////////////////////////////////////////////////////////////////////////////////////////
                $name = $this->db_connection->real_escape_string(strip_tags($_POST['name'], ENT_QUOTES));
				$address = $this->db_connection->real_escape_string(strip_tags($_POST['address'], ENT_QUOTES));
				$qual = $this->db_connection->real_escape_string(strip_tags($_POST['qual'], ENT_QUOTES));
				$gen = $this->db_connection->real_escape_string(strip_tags($_POST['gen'], ENT_QUOTES));
				$patho_name = $this->db_connection->real_escape_string(strip_tags($_POST['patho_name'], ENT_QUOTES));
				$email = $this->db_connection->real_escape_string(strip_tags($_POST['email'], ENT_QUOTES));

				$tel = $this->db_connection->real_escape_string(strip_tags($_POST['tel'], ENT_QUOTES));
				$bloodg = $this->db_connection->real_escape_string(strip_tags($_POST['bloodg'], ENT_QUOTES));
				
				

                $patho_password = $_POST['patho_password_new'];

               
                $patho_password_hash = password_hash($patho_password, PASSWORD_DEFAULT);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // check if user or email address already exists
                $sql = "SELECT * FROM pathologist WHERE patho_name = '" . $patho_name . "'";
                $query_check_patho_name = $this->db_connection->query($sql);

                if ($query_check_patho_name->num_rows == 1) {
                    $this->errors[] = "Sorry, that username / email address is already taken.";
                } else {
                    // write new user's data into database
////////////////////////					/////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    $sql = "INSERT INTO pathologist (name, user_password_hash, address, qual, gender, patho_name,email, tel, bloodg)
                           VALUES('".$name . "', 
						   '" . $patho_password_hash . "',
						   
						   '" . $address . "',
						   '" . $qual . "',
						   '" . $gen . "',
						   '" . $patho_name . "',
						   
						   '".$email."',
						   '" . $tel . "',
						  
						   '" . $bloodg . "');";
					$query_new_user_insert = $this->db_connection->query($sql);

                    // if user has been added successfully
                    if ($query_new_user_insert) {
                        $this->messages[] = "New pathologist  has been added successfully.";
                    } else {
                        $this->errors[] = "Sorry, your registration failed. Please go back and try again.";
                    }
                }
            } else {
                $this->errors[] = "Sorry, no database connection.";
            }
        
		//else {
          //  $this->errors[] = "An unknown error occurred.";
        //}
    }
	}

