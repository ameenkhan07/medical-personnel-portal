<!DOCTYPE html>
<?php date_default_timezone_set('Asia/Calcutta');
    session_start();?>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="css/style2.css">
  <style type="text/css">

.popup{
display: none;
position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background-color: rgba(17,17,17,.8);
overflow: auto;
z-index: 10000;
}  
.popup img{
    position: relative;
    top: 50%;
    transform: translateY(-50%);
}
@keyframes zoom {
    from {
        width: 0;
        height: 0;
        left:50%;
        top:50%;
    }
    to {
        width: 100%;
        height: 100%;
        top:0%;
        left: 0%;
    }
}

@keyframes fade {
   from {
         width: 100%;
        height: 100%;
        top:0%;
        left: 0%;
    }
    to {
       width: 0;
        height: 0;
        left:50%;
        top:50%;
    }
}

.menubox{
    text-align:center;margin:2%;width:130px;height:30px;border-width:1px; border-color:#fff; border-style:solid; float :left;font-size: 12px;
    letter-spacing: 0.6px;font-family: 'proxima-nova', 'Calibri', 'Cantarell', sans-serif;color:#fff

}
.menubox:hover{
    text-align:center;margin:2%;width:130px;height:30px;border-width:1px; border-color:#fff; border-style:solid; float :left;font-size: 12px;
    letter-spacing: 0.6px;font-family: 'proxima-nova', 'Calibri', 'Cantarell', sans-serif;color:#fff; background-color: rgba(181, 174, 174, 0.67);

}
#fileContainer {
     position: absolute;
     top: 60px;
     width: 100%;
}

#fileContainer img{
    cursor: pointer;
}

#filebox {
     position: absolute;
     right: 0px;
     width: calc(100% - 230px);
}

#sideNav {
     position: absolute;
     top: 0px;
     width: 200px;
}

#sideNav button {
    width: 160px;
    background-color: #428bca;
    border-color: #357ebd;
    color: #fff;
    margin: 1px;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
    cursor: pointer;
    display: inline-block;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857;
    margin-bottom: 0;
    padding: 6px 12px;
    text-align: center;
    vertical-align: middle;
    white-space: nowrap;
}



</style>

<script type="text/javascript">

var sid = 'userfilebox';

document.onclick = function(event){
    if(event.target.classList.contains('image')){
        el = document.getElementById('imageViewPopup');
        el.innerHTML = '<img src="'+event.target.src+'">';
        el.style.display='block'; 
        el.style.animation = 'zoom 0.5s';
        el.style.width='100%';
        el.style.height='100%';
    }else if(event.target.classList.contains('popup')) {
        el.style.animation = 'fade 0.5s';
        el.style.width='0';
        el.style.height='0';
    }
}

function showUploadPopup(){
    document.getElementById('fileUploadDiv').style.display="block";
}
function changeFiles(id,el){
    if(id == sid) return;
    document.getElementById(id).style.display = 'block';
    document.getElementById(sid).style.display = 'none';
    sb.style.backgroundColor = '#428bca';
    sb.style.borderColor = '#357ebd';
    sb = el;
    sb.style.backgroundColor = '#5cb85c';
    sb.style.borderColor = '#4cae4c';
    sid = id;
}

  function uploadNewFile(){
    if(!document.getElementById('fileInput').value) {
      alert("File Not Selected");
      return;
  }

  fileUploadProgressBar = document.getElementById('fileUploadProgress');

  ajax = new XMLHttpRequest();
  data = new FormData(document.forms.namedItem("fileForm"));
  data.append('aid',<?php echo $_POST['appoint'] ?>);

  ajax.onload = function(){
      if(ajax.readyState==4 && ajax.status==200){
        alert(ajax.responseText);
        fetchFiles();
    }
}

ajax.upload.onprogress = function (event){
  if (event.total != 0){
    fileUploadProgressBar.value = (event.loaded * 100) / event.total;
}else{
    fileUploadProgressBar.removeAttribute('value');
}
}
ajax.open('POST',"uploadwa_file.php",true);
ajax.send(data);
}

function setName(el){
    ar = el.value.split('\\');
    document.getElementById('fileName').value = ar[ar.length-1];
}

</script>
</head>
<body style="background-color: #e9eaed; padding:0px; margin:0px">
  <header style='background-color: #3b9885; width:100%;height:50px;position:fixed; top:-1px; z-index:1000' >


    <?php
    

    $dbhost = 'localhost';
// Unlikely to require changing
    $dbname = 'orfom';
// Modify these...
    $dbuser_name = 'orfom';
// ...variables according
    $dbpass = 'awerty56';
// ...to your installation

    mysql_connect($dbhost, $dbuser_name, $dbpass) or die(mysql_error());
    mysql_select_db($dbname) or die(mysql_error());

    include 'function.php';

    if (isset($_SESSION['user_name'])) {
      $user_name = $_SESSION['user_name'];
      $loggedin = TRUE;
      $user_namestr = " ($user_name)";
  } else
  $loggedin = FALSE;
  if($loggedin)
  {
      $subquery = "SELECT * FROM users WHERE user_name='$user_name'";
      $subresult = mysql_query($subquery);
      if (!$subresult)
        die("Database access failed: " . mysql_error());
    $subrow = mysql_fetch_row($subresult);
    $uid =$subrow[0];
    echo  "<div> </div><span class='profilepic'><img src='images/$user_name.jpg' style='width:32px; height: 32px;margin-top:8px; border-radius:3px;'/></span> 
    <span class='profilename' > Mr. $user_name</span><span class='profileid'> ID: $uid</span><span class='navmenuwrapper'> </span>";
    echo "<div style='position: relative;margin-left: 36%;margin-top: -53px; '>
    <a href='userpage.php' ><div class= 'menubox'><div style='margin-top:8px'>Appointment</div></div></a>
    <a href='ordermedicine' ><div class= 'menubox'><div style='margin-top:8px'>Find Chemist</div></div></a>
    <a href='findPatho' ><div class= 'menubox'><div style='margin-top:8px'>Find Labs</div></div></a>
    <a href='timeline.php' ><div class= 'menubox'><div style='margin-top:8px'>Timeline</div></div></a></div>  
    <div class='settings'><a href='ulogout.php' ><img src='images/settings2.png' style='border-radius:3px;' ></a></div>
    ";

}
?>
<?php 
if(!$loggedin){

  echo   "<div class='arbit'> </div><div style='position:relative;  margin-left:60%;padding-top:0px '>
  <div class= 'menubox'>
    <div style='margin-top:8px'>GET LISTED</div>
</div>
<div class= 'menubox'>
    <div style='margin-top:8px'>CONTACT US</div>
</div>
<a href='http://localhost/~praveenkumar/orfom/login/user_login/uindex.php' ><div class= 'menubox'>
    <div style='margin-top:8px'>LOGIN</div>
</div></a>
</div>  
";
}?>
</header>

<div id="fileContainer"> 
    <div id="sideNav">
        <button id="fButton" onclick="changeFiles('userfilebox',this)"
            style="background-color: #5cb85c;border-color: #4cae4c;">
            Files By Me
        </button>
        <script type="text/javascript">
        var sb = document.getElementById('fButton');
        </script>
        <button onclick="changeFiles('pathofilebox',this)">Files By Dr</button>
        <button onclick="changeFiles('docfilebox',this)">Files By Lab</button>
    </div>
    <div id="filebox">
        <fieldset id="userfilebox">
            <legend>Files By Me</legend>
            <?php
            $imageFileTypes = array('jpg', 'JPG', 'png' ,'PNG' ,'jpeg' ,'JPEG','gif','GIF');
            $path = "doc_presc/uploads/user/".join('/',str_split(strval($_POST['appoint'])));
            $ar = scandir($path);
            $n = count($ar);
            for($i=0;$i<$n;$i++){
              if(is_file($path.'/'.$ar[$i])) {
                $ext = explode('.',$ar[$i]);
                $ext = $ext[1];
                if(in_array($ext, $imageFileTypes)) {
                  echo '<img class="image" width="128" height="128" src="'.$path.'/'.$ar[$i].'">';
                }else{
                  echo '<a href="'.$path.'/'.$ar[$i].'"><img width="128" height="128" src="images/fileicon.png"></a>';
                }
              }
            }
            ?>
            <img width="128" height="128" src="images/newfile.png" onclick="showUploadPopup()">
        </fieldset>
        <fieldset id="pathofilebox" style="display:none">
            <legend>Files By Me</legend>
            <?php
            $imageFileTypes = array('jpg', 'JPG', 'png' ,'PNG' ,'jpeg' ,'JPEG','gif','GIF');
            $path = "doc_presc/uploads/patho/".join('/',str_split(strval($_POST['appoint'])));
            $ar = scandir($path);
            $n = count($ar);
            for($i=0;$i<$n;$i++){
              if(is_file($path.'/'.$ar[$i])) {
                $ext = explode('.',$ar[$i]);
                $ext = $ext[1];
                if(in_array($ext, $imageFileTypes)) {
                  echo '<img class="image" width="128" height="128" src="'.$path.'/'.$ar[$i].'">';
                }else{
                  echo '<a href="'.$path.'/'.$ar[$i].'"><img width="128" height="128" src="images/fileicon.png"></a>';
                }
              }
            }
            ?>
        </fieldset>
        <fieldset id="docfilebox" style="display:none">
            <legend>Files By Me</legend>
            <?php
            $imageFileTypes = array('jpg', 'JPG', 'png' ,'PNG' ,'jpeg' ,'JPEG','gif','GIF');
            $path = "doc_presc/uploads/doc/".join('/',str_split(strval($_POST['appoint'])));
            $ar = scandir($path);
            $n = count($ar);
            for($i=0;$i<$n;$i++){
              if(is_file($path.'/'.$ar[$i])) {
                $ext = explode('.',$ar[$i]);
                $ext = $ext[1];
                if(in_array($ext, $imageFileTypes)) {
                  echo '<img class="image" width="128" height="128" src="'.$path.'/'.$ar[$i].'">';
                }else{
                  echo '<a href="'.$path.'/'.$ar[$i].'"><img width="128" height="128" src="images/fileicon.png"></a>';
                }
              }
            }
            ?>
        </fieldset>
</div>
<div id='fileUploadDiv' style="display:none">
    <form name="fileForm" enctype="multipart/form-data">
      <input type="text" id="fileName" placeholder="Name" name="name">
      <input type="file" id="fileInput" name="upfile" onchange = "setName(this)">
      <button onclick = "uploadNewFile()">Upload</button>
      File Upload Progress:
      <progress id="fileUploadProgress" min="0" max="100" value="0"></progress>
  </form>
</div>
<div class="popup" align="center" id="imageViewPopup">
    

</div>
