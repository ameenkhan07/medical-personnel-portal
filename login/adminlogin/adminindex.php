<html>
<head>
<link rel="stylesheet" type="text/css" href="../../css/style.css">

</head>
<body >
<div align="center">
<div class="centreimage"></div>

<?php 
if (version_compare(PHP_VERSION, '5.3.7', '<')) {
    exit("Sorry, Simple PHP Login does not run on a PHP version smaller than 5.3.7 !");
} else if (version_compare(PHP_VERSION, '5.5.0', '<')) {
    
    require_once("libraries/password_compatibility_library.php");
}


require_once("config/db.php");
require_once("classes/AdminLogin.php");

$login = new AdminLogin();


if ($login->isUserLoggedIn() == true) {
     header('Location: ../../admin/register/adminmain.php');

} else {
    
    include("views/admin_not_logged_in.php");
}


?>
</div>
<div align="center">
<a href="#" style="text-decoration:none"><span class="cranacc1" >Forgot your password? </span></a> 
<span class="seperator"></span>
<a href="adminregister.php" style="text-decoration:none"><span class="cranacc" >Create an account</span></a>
 

 <a href="../../index.php" style="text-decoration:none"> <span class="backimage1"> <img src="../../img/back.png"> Back </span>

</div>
</body>
</html>
