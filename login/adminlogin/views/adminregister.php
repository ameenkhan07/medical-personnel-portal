<html>
<head>
<link rel="stylesheet" type="text/css" href="../../css/style3.css">
</head>
<body>
<div class="error">
<?php
// show potential errors / feedback (from registration object)
if (isset($registration)) {
    if ($registration->errors) {
        foreach ($registration->errors as $error) {
            echo $error;
        }
    }
    if ($registration->messages) {
        foreach ($registration->messages as $message) {
            echo $message;
        }
    }
}
?>
</div>
<!-- register form -->
<form method="post" action="adminregister.php" name="registerform" id="form1">

    <div class="username"> Name </div> <div class="usernameerrortext"></div> <div class="usernameerrorsymbol"></div>
    <input id="login_input_username" class="login_input" type="text" pattern="[a-zA-Z0-9]{2,64}" name="name" required value="<?php echo (isset($_POST['name']) ? $_POST['name'] : ''); ?>">
<div class="username"> User Name </div> <div class="usernameerrortext"></div> <div class="usernameerrorsymbol"></div>
    <input id="login_input_username" class="login_input" type="text" pattern="[a-zA-Z0-9]{2,64}" name="user_name" required value="<?php echo (isset($_POST['user_name']) ? $_POST['user_name'] : ''); ?>">

    
    <div class="username"> Email_Id</div> <div class="usernameerrortext"></div> <div class="usernameerrorsymbol"></div>
    <input id="login_input_username" class="login_input" type="text"  name="email" required value="<?php echo (isset($_POST['email']) ? $_POST['email'] : ''); ?>">

	
	<div class="username"> Telephone </div> <div class="usernameerrortext"></div> <div class="usernameerrorsymbol"></div>
    <input id="login_input_usernumber" class="login_input" type="number"  name="tel" required value="<?php echo (isset($_POST['tel']) ? $_POST['tel'] : ''); ?>">
	  
    
	<div class="username"> Hospital ID </div> <div class="usernameerrortext"></div> <div class="usernameerrorsymbol"></div>
    <input id="login_input_usernumber" class="login_input" type="number"  name="hid" required value="<?php echo (isset($_POST['hid']) ? $_POST['hid'] : ''); ?>">
	  
    
    <div class="username"> Password (min. 6 characters)</div> <div class="usernameerrortext"></div> <div class="usernameerrorsymbol"></div>
    <input id="login_input_password_new" class="login_input" type="password" name="user_password_new" pattern=".{6,}" required autocomplete="off" value="<?php echo (isset($_POST['user_password_new']) ? $_POST['user_password_new'] : ''); ?>">

    <div class="username"> Repeat password</div> <div class="usernameerrortext"></div> <div class="usernameerrorsymbol"></div>
    <input id="login_input_password_repeat" class="login_input" type="password" name="user_password_repeat" pattern=".{6,}" required autocomplete="off" value="<?php echo (isset($_POST['user_password_repeat']) ? $_POST['user_password_repeat'] : ''); ?>">
    


</form>
<div class="enterimage">
    <input type="submit"  form="form1" name="register" value="" ></div>
<!-- backlink -->
<div class="back"><a href="../../login/adminlogin/adminindex.php" style="color:#fff; text-decoration:none"> Back <img src="../../img/back.png" style="margin:-2px"> </div>
</body>
</html>
