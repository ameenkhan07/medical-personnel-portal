<html>
<head>
<link rel="stylesheet" type="text/css" href="../../css/style3.css">
</head>
<body>
<div class="error">
<?php
// show potential errors / feedback (from registration object)
if (isset($registration)) {
    if ($registration->errors) {
        foreach ($registration->errors as $error) {
            echo $error;
        }
    }
    if ($registration->messages) {
        foreach ($registration->messages as $message) {
            echo $message;
        }
    }
}
?>
</div>
<!-- register form -->
<form method="post" action="recregister.php" name="registerform" id="form1">

         <div class="username"> Name </div> <div class="usernameerrortext"></div> <div class="usernameerrorsymbol"></div>
    <input id="login_input_username" class="login_input" type="text" pattern="[a-zA-Z0-9]{2,64}" name="name" required >

    <div class="username"> Address</div> <div class="usernameerrortext"></div> <div class="usernameerrorsymbol"></div>
    <input id="login_input_usernumber" class="login_input" type="text"  name="address" required >
    
    <div class="username"> Email</div> <div class="usernameerrortext"></div> <div class="usernameerrorsymbol"></div>
    <input id="login_input_usernumber" class="login_input" type="text"  name="email" required >
      
     <div class="username"> qualification</div> <div class="usernameerrortext"></div> <div class="usernameerrorsymbol"></div>
    <input id="login_input_usernumber" class="login_input" type="text"  name="qual" required >
      
      <div class="username"> gender</div> <div class="usernameerrortext"></div> <div class="usernameerrorsymbol"></div>
    <input id="login_input_usernumber" class="login_input" type="text"  name="gen" required >
      
      <div class="username"> Username</div> <div class="usernameerrortext"></div> <div class="usernameerrorsymbol"></div>
    <input id="login_input_usernumber" class="login_input" type="text"  name="recept_name" required >
         
           
    <div class="username"> Telephone</div> <div class="usernameerrortext"></div> <div class="usernameerrorsymbol"></div>
    <input id="login_input_usernumber" class="login_input" type="number"  name="tel" required >
    
    <div class="username"> age</div> <div class="usernameerrortext"></div> <div class="usernameerrorsymbol"></div>
    <input id="login_input_usernumber" class="login_input" type="number"  name="age" required >
    
    
    <div class="username"> blood group</div> <div class="usernameerrortext"></div> <div class="usernameerrorsymbol"></div>
    <input id="login_input_usernumber" class="login_input" type="text"  name="bloodg" required >
     

    <div class="username"> Password (min. 6 characters)</div> <div class="usernameerrortext"></div> <div class="usernameerrorsymbol"></div>
    <input id="login_input_password_new" class="login_input" type="password" name="recept_password_new" pattern=".{6,}" required autocomplete="off" >

    <div class="username"> Repeat password</div> <div class="usernameerrortext"></div> <div class="usernameerrorsymbol"></div>
    <input id="login_input_password_repeat" class="login_input" type="password" name="recept_password_repeat" pattern=".{6,}" required autocomplete="off" >

</form>
<div class="enterimage">
    <input type="submit"  form="form1" name="register" value="" ></div>
<!-- backlink -->
<div class="back"><a href="../../login/Reclogin/recindex.php" style="color:#fff; text-decoration:none"> Back <img src="../../img/back.png" style="margin:-2px"></div>
</body>
</html>