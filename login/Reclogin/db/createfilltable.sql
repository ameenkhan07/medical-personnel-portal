CREATE TABLE IF NOT EXISTS `login`.`users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'auto incrementing user_id of each user, unique index',
  `user_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s name, unique',
  `user_address` varchar(164) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s address',
  `user_number` int(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s number' ,
  `user_password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s password in salted and hashed format',
  `user_email` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s email, unique',
  `user_bg` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s bg',
  `user_age` int(3) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s age',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`),
  UNIQUE KEY `user_email` (`user_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='user data';
