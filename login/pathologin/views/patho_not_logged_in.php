<?php
if (isset($loginp)) {
    if ($loginp->errors) {
        foreach ($loginp->errors as $error) {
            echo $error;
        }
    }
    if ($loginp->messages) {
        foreach ($loginp->messages as $message) {
            echo $message;
        }
    }
}
?>


<form method="post" action="pathoindex.php" name="loginform">

    <label for="login_input_username">Username</label>
    <input id="login_input_username" class="login_input" type="text" name="patho_name" required />

    <label for="login_input_password">Password</label>
    <input id="login_input_password" class="login_input" type="password" name="user_password" autocomplete="off" required />

    <input type="submit"  name="loginp" value="Log in" />

</form>

<a href="pathoregister.php">Register new account</a>
