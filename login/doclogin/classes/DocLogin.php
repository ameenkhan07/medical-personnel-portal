<?php


class DocLogin
{
    
    private $db_connection = null;
   
    public $errors = array();
   
    public $messages = array();

   
    public function __construct()
    {
        
        session_start();

        
        if (isset($_GET["logoutd"])) {
            $this->doLogout();
        }
        elseif (isset($_POST["logind"])) {
            $this->dologinWithPostData();
        }
    }

   
    private function dologinWithPostData()
    {
        // check login form contents
        if (empty($_POST['doc_name'])) {
            $this->errors[] = "Username field was empty.";
        } elseif (empty($_POST['doc_password'])) {
            $this->errors[] = "Password field was empty.";
        } elseif (!empty($_POST['doc_name']) && !empty($_POST['doc_password'])) {

            // create a database connection, using the constants from config/db.php (which we loaded in index.php)
            $this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

            // change character set to utf8 and check it
            if (!$this->db_connection->set_charset("utf8")) {
                $this->errors[] = $this->db_connection->error;

            }

            // if no connection errors 
            if (!$this->db_connection->connect_errno) {

                // escape the POST stuff
                $doc_name = $this->db_connection->real_escape_string($_POST['doc_name']);
                


    
                // database query, getting all the info of the selected user (allows login via email address in the
                // username field)
///////////////////////////////////				////////////////////////////////////////////////////////////////////////////////////////////////////
                $sql = "SELECT doc_name, doc_password_hash
                        FROM doctors_info
                        WHERE doc_name = '" . $doc_name . "' ";
                $result_of_login_check = $this->db_connection->query($sql);

                // if this user exists
                if ($result_of_login_check->num_rows == 1) {

                    // get result row (as an object)
                    $result_row = $result_of_login_check->fetch_object();

                    
                    if (password_verify($_POST['doc_password'], $result_row->doc_password_hash)) {

                        // write user data into PHP SESSION (a file on your server)
                        $_SESSION['doc_name'] = $result_row->doc_name;
                        //$_SESSION['user_email'] = $result_row->user_email;
                        $_SESSION['doc_login_status'] = 1;

                    } else {
                        $this->errors[] = "Wrong password. Try again.";
                    }
                } else {
                    $this->errors[] = "This user does not exist.";
                }
            } else {
                $this->errors[] = "Database connection problem.";
            }
        }
    }

   
    public function doLogout()
    {
        // delete the session of the user
        $_SESSION = array();
        session_destroy();
    
        $this->messages[] = "You have been logged out.";

    }

  
    public function isDocLoggedIn()
    {
        if (isset($_SESSION['doc_login_status']) AND $_SESSION['doc_login_status'] == 1) {
            return true;
        }
        // default return
        return false;
    }
}
